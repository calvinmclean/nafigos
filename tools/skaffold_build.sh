#!/usr/bin/env bash
set -e

cd ../
docker build . -t $IMAGE -f $1/Dockerfile
