package main

import (
	"os"
	"sync"

	"gitlab.com/cyverse/nafigos/common"
	"gitlab.com/cyverse/nafigos/workflow-definition-service/workflowdefinition"

	log "github.com/sirupsen/logrus"
)

const (
	defaultClusterID   = "nafigos-cluster"
	defaultClientID    = "workflow-definition"
	defaultConnAddress = "nats://localhost:4222"
)

var natsInfo map[string]string

func init() {
	log.SetFormatter(&log.JSONFormatter{})
	log.SetOutput(os.Stdout)
	level, err := log.ParseLevel(os.Getenv("LOG_LEVEL"))
	if err != nil {
		level = log.InfoLevel
	}
	log.SetLevel(level)

	err = workflowdefinition.InitMongoDB(
		os.Getenv("MONGODB_DB_NAME"),
		os.Getenv("MONGODB_ADDRESS"),
	)
	if err != nil {
		log.WithFields(log.Fields{
			"package":  "main",
			"function": "init",
			"error":    err,
		}).Panic("unable to connect to MongoDB")
	}
	log.WithFields(log.Fields{
		"package":  "main",
		"function": "init",
	}).Info("successfully connected to MongoDB")

	natsInfo = common.GetNATSInfo(defaultClusterID, defaultClientID, defaultConnAddress)
}

func main() {
	var wg sync.WaitGroup
	wg.Add(6)
	go common.StreamingQueueSubscriber("WorkflowDefinition.Create", "WorkflowDefinition.CreateQueue", workflowdefinition.Create, natsInfo, &wg)
	go common.StreamingQueueSubscriber("WorkflowDefinition.Delete", "WorkflowDefinition.DeleteQueue", workflowdefinition.Delete, natsInfo, &wg)
	go common.StreamingQueueSubscriber("WorkflowDefinition.Update", "WorkflowDefinition.UpdateQueue", workflowdefinition.Update, natsInfo, &wg)
	go common.SynchronousSubscriber("WorkflowDefinition.Get", "WorkflowDefinition.GetQueue", workflowdefinition.Get, natsInfo, &wg)
	go common.SynchronousSubscriber("WorkflowDefinition.GetForUser", "WorkflowDefinition.GetForUserQueue", workflowdefinition.GetForUser, natsInfo, &wg)
	wg.Wait()
}
