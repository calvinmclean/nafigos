package types

import (
	"fmt"
	"strings"

	"gitlab.com/cyverse/nafigos/common"

	"github.com/openfaas/faas-cli/schema"
	openfaasv1alpha2 "github.com/openfaas/faas-cli/schema/openfaas/v1alpha2"
	"github.com/openfaas/faas-cli/stack"
	openfaas "github.com/openfaas/faas-netes/pkg/apis/openfaas/v1"
	api "istio.io/api/networking/v1beta1"
	istio "istio.io/client-go/pkg/apis/networking/v1beta1"
	core "k8s.io/api/core/v1"
	meta "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/yaml"
)

const (
	resourceKind      = "Function"
	defaultAPIVersion = "openfaas.com/v1alpha2"
)

// OpenFaaSWorkflow ...
type OpenFaaSWorkflow struct {
	Services *stack.Services
}

// NewOpenFaaSWorkflow ...
func NewOpenFaaSWorkflow(parsedWorkflow []byte) (NafigosWorkflow, error) {
	services, err := stack.ParseYAMLData(parsedWorkflow, "", "", false)
	if err != nil {
		return OpenFaaSWorkflow{}, err
	}
	return OpenFaaSWorkflow{services}, nil
}

// Type returns the name of this Workflow Type
func (faas OpenFaaSWorkflow) Type() string {
	return "OpenFaaSWorkflow"
}

// Run ...
func (faas OpenFaaSWorkflow) Run(clientsets *common.K8sClientsets, request map[string]interface{}, name, namespace string) error {
	id := request["id"].(string)
	version := request["version"].(string)

	// Get Function CRDs from OpenFaaS YAML
	fns, err := parseOpenFaaSFunctions(*faas.Services, id, version)
	if err != nil {
		return err
	}

	// Loop through each function and run
	for i, fn := range fns {
		fn.ObjectMeta.Name = createIndexedName(name, i)
		fn.ObjectMeta.Labels = map[string]string{
			"app":        createIndexedName(name, i),
			"version":    version,
			"nafigos_id": id,
		}

		_, err = clientsets.CreateFunction(&fn, namespace)
		if err != nil {
			return err
		}
	}

	return err
}

// SetupRouting ...
func (faas OpenFaaSWorkflow) SetupRouting(clientsets *common.K8sClientsets, adminClientsets *common.K8sClientsets, request map[string]interface{}, name, namespace string, cluster common.Cluster) error {
	id := request["id"].(string)
	version := request["version"].(string)
	httpPorts := request["httpPorts"].([]core.ServicePort)

	// Get Function CRDs from OpenFaaS YAML
	fns, err := parseOpenFaaSFunctions(*faas.Services, id, version)
	if err != nil {
		return err
	}

	// Loop through each function and run
	var routes []*api.HTTPRoute
	for i, fn := range fns {
		// Create DestinationRules for each Function
		dr := &istio.DestinationRule{
			ObjectMeta: meta.ObjectMeta{
				Name: createIndexedName(name, i),
				Labels: map[string]string{
					"app":           createIndexedName(name, i),
					"version":       version,
					"nafigos_id":    id,
					"faas_function": fn.Spec.Name,
				},
			},
			Spec: api.DestinationRule{
				Host: fn.Spec.Name,
				Subsets: []*api.Subset{
					{
						Name: version,
						Labels: map[string]string{
							"faas_function": fn.Spec.Name,
						},
					},
				},
			},
		}
		_, err = clientsets.CreateDestinationRule(dr, namespace)
		if err != nil {
			return err
		}

		// Prepare routes for VirtualService
		port := httpPorts[0].Port
		path := fmt.Sprintf("/%s/%s:%d", id, fn.Spec.Name[0:len(fn.Spec.Name)-21], port)
		routes = append(routes, createHTTPRoute(fn.Spec.Name, version, path, port))
	}

	vs := createVirtualService(name, request["host"].(string), version, id, routes, nil)
	_, err = clientsets.CreateVirtualService(vs, namespace)
	if err != nil {
		return err
	}

	return err
}

// Delete ...
func (faas OpenFaaSWorkflow) Delete(clientsets *common.K8sClientsets, name, namespace string) error {
	// Loop through each function and delete it
	for i := 0; i < len(faas.Services.Functions); i++ {
		err := clientsets.DeleteFunction(createIndexedName(name, i), namespace)
		if err != nil {
			return err
		}

		err = clientsets.DeleteDestinationRule(createIndexedName(name, i), namespace)
		if err != nil {
			return err
		}
	}

	err := clientsets.DeleteVirtualService(name, namespace)
	if err != nil {
		return err
	}

	return nil
}

// ScaleFromZero ...
func (faas OpenFaaSWorkflow) ScaleFromZero(clientsets *common.K8sClientsets, request map[string]interface{}, name, namespace string) error {
	return faas.Run(clientsets, request, name, namespace)
}

func createIndexedName(name string, n int) string {
	result := name + fmt.Sprintf("-%d", n)
	// If long name is too long for K8s, shorten it before appending n
	if len(result) > 63 {
		result = name[0:len(name)-(len(result)-63)] + fmt.Sprintf("-%d", n)
	}
	return result
}

func parseOpenFaaSFunctions(services stack.Services, id, version string) (fns []openfaas.Function, err error) {
	// Translate OpenFaaS functions to CRD YAML
	crdYAML, err := generateCRDYAML(services, id, version)
	if err != nil {
		return
	}

	// Now unmarshal into actual Function CRD struct
	err = yaml.Unmarshal([]byte(crdYAML), &fns)
	return
}

func generateCRDYAML(services stack.Services, id, version string) (string, error) {
	var result string
	if len(services.Functions) > 0 {
		for funcName, function := range services.Functions {
			imageName := schema.BuildImageName(0, function.Image, "", "")

			// Add new labels and preserve existing labels
			labels := map[string]string{
				"version":       version,
				"nafigos_id":    id,
				"faas_function": funcName + fmt.Sprintf("-%s", id),
			}
			if function.Labels != nil {
				for k, v := range *function.Labels {
					labels[k] = v
				}
			}
			function.Labels = &labels

			crd := openfaasv1alpha2.CRD{
				APIVersion: defaultAPIVersion,
				Kind:       resourceKind,
				Spec: openfaasv1alpha2.Spec{
					Name:        funcName + fmt.Sprintf("-%s", id),
					Image:       imageName,
					Environment: function.Environment,
					Labels:      function.Labels,
					Limits:      function.Limits,
					Requests:    function.Requests,
					Constraints: function.Constraints,
					Secrets:     function.Secrets,
				},
			}

			//Marshal the object definition to yaml
			crdYAML, err := yaml.Marshal(crd)
			if err != nil {
				return "", err
			}
			// Replace all newlines (except for last one) with newline and 2 spaces to indent
			newlines := strings.Count(string(crdYAML), "\n")
			result += "- " + strings.Replace(string(crdYAML), "\n", "\n  ", newlines-1)
		}
	}

	return result, nil
}
