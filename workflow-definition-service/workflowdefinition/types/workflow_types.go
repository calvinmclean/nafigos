package types

import (
	"bytes"
	"fmt"
	"text/template"

	"gitlab.com/cyverse/nafigos/common"
)

// NafigosWorkflow is an interface used to allow Pleo to simply run WorkflowDefinition.Run
// in order to create and deploy all the necessary Kubernetes resources. This makes it
// simpler to add new WorkflowTypes
type NafigosWorkflow interface {
	Type() string
	Run(clientsets *common.K8sClientsets, request map[string]interface{}, name, namespace string) error
	SetupRouting(clientsets *common.K8sClientsets, adminClientsets *common.K8sClientsets, request map[string]interface{}, name, namespace string, cluster common.Cluster) error
	Delete(clientsets *common.K8sClientsets, name, namespace string) error
	ScaleFromZero(clientsets *common.K8sClientsets, request map[string]interface{}, name, namespace string) error
}

// WorkflowFactory is used to get the constructor for the correct type of Workflow
// and parse the bytes to create a NafigosWorkflow
func WorkflowFactory(workflowType string, workflowBytes []byte) (NafigosWorkflow, error) {
	switch workflowType {
	case "argo":
		return NewArgoWorkflow(workflowBytes)
	case "mock":
		return NewMockWorkflow(workflowBytes)
	case "container", "default":
		return NewContainerWorkflow(workflowBytes)
	case "openfaas":
		return NewOpenFaaSWorkflow(workflowBytes)
	default:
		return nil, fmt.Errorf("no constructor for Workflow Type '%s'", workflowType)
	}
}

// ParseRawWorkflow will read the RawWorkflow string and create the correct
// type of NafigosWorkflow from it
func ParseRawWorkflow(rawWorkflow, workflowType string, buildSteps []*common.BuildStep) (workflow NafigosWorkflow, err error) {
	// Parse Workflow template to fill out image names
	parsedWorkflow, err := parseWorkflowTemplate(rawWorkflow, buildSteps)
	if err != nil {
		err = fmt.Errorf("Error parsing Workflow template: %v", err)
		return
	}

	// Different processes for Workflow types
	workflow, err = WorkflowFactory(workflowType, parsedWorkflow)
	if err != nil {
		err = fmt.Errorf("Error creating '%s' Workflow from string: %v", workflowType, err)
		return
	}
	return
}

// parseWorkflowTemplate is used to read the second part of a WorkflowDefinition when a
// user defines images based on the image built from the first part of the WFD.
// The template should look like "{{image N}}" where N is the index of the image
// in the build list. Starts at 1
func parseWorkflowTemplate(text string, builds []*common.BuildStep) ([]byte, error) {
	// Create "image" function to get Image name from BuildStep n
	funcMap := template.FuncMap{
		"image": func(n int) string {
			return builds[n-1].Image
		},
	}
	// Create template and parse input text
	t, err := template.New("template").Funcs(funcMap).Parse(text)
	if err != nil {
		return []byte{}, err
	}
	// Execute template and write to buffer
	var result bytes.Buffer
	err = t.Execute(&result, builds)
	if err != nil {
		return []byte{}, err
	}
	return result.Bytes(), nil
}

/*
 * Common functions used by WorkflowTypes
 */
