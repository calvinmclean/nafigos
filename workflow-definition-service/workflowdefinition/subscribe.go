package workflowdefinition

import (
	"encoding/json"
	"fmt"

	"gitlab.com/cyverse/nafigos/common"

	"github.com/nats-io/nats.go"
	log "github.com/sirupsen/logrus"
)

// Request is a struct containing the necessary fields for all WorkflowDefinition
// operations coming from the API
type Request struct {
	common.BasicRequest
	WorkflowDefinition
	URL           string `json:"url,omitempty"`
	Branch        string `json:"branch,omitempty"`
	CommitMessage string `json:"commit_msg,omitempty"`
}

// Create is used to create a new WorkflowDefinition
func Create(reqBytes []byte, natsInfo map[string]string) {
	logger := log.WithFields(log.Fields{
		"package":  "workflowdefinition",
		"function": "Create",
	})

	var request Request
	err := json.Unmarshal(reqBytes, &request)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to unmarshal JSON bytes into Request")
		return
	}

	logRequest := request
	logRequest.BasicRequest = logRequest.GetRedacted()
	logger = logger.WithFields(log.Fields{"request": logRequest})

	if len(request.GetUser().Username) == 0 {
		logger.WithFields(log.Fields{"error": err}).Error("user is required to create WorkflowDefinition but was not provided")
		return
	}
	_, err = define(
		request.GetWorkflowID(),
		request.URL,
		request.Branch,
		request.GetUser(),
		request.GetGitSecretID(),
	)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to create WorkflowDefinition")
		return
	}
}

// Delete is used to delete and existing workflowdefinition
func Delete(reqBytes []byte, natsInfo map[string]string) {
	logger := log.WithFields(log.Fields{
		"package":  "workflowdefinition",
		"function": "Delete",
	})

	var request Request
	err := json.Unmarshal(reqBytes, &request)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to unmarshal JSON bytes into Request")
		return
	}

	logRequest := request
	logRequest.BasicRequest = logRequest.GetRedacted()
	logger = logger.WithFields(log.Fields{"request": logRequest})

	wfd, err := get(request.GetWorkflowID(), request.GetUser().Username)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to get WorkflowDefinition before deleting")
		return
	}

	logger.Info("deleting WorkflowDefinition")
	err = wfd.delete()
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to delete WorkflowDefinition")
		return
	}
}

// Update allows a user to edit a WorkflowDefinition and push to git
func Update(reqBytes []byte, natsInfo map[string]string) {
	logger := log.WithFields(log.Fields{
		"package":  "workflowdefinition",
		"function": "Update",
	})

	var request Request
	err := json.Unmarshal(reqBytes, &request)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to unmarshal JSON bytes into Request")
		return
	}

	logRequest := request
	logRequest.BasicRequest = logRequest.GetRedacted()
	logger = logger.WithFields(log.Fields{"request": logRequest})

	wfd, err := get(request.GetWorkflowID(), request.GetUser().Username)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to get WorkflowDefinition before updating")
		return
	}

	// First make sure our WFD reflects git state before editing
	err = wfd.update(request.GetUser(), request.GetGitSecretID())
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to update WorkflowDefinition from git")
		return
	}

	if len(request.GetGitSecretID()) == 0 {
		err = fmt.Errorf("GitSecretID not provided")
		logger.WithFields(log.Fields{"error": err}).Error("unable to update WorkflowDefinition without git Secret")
		return
	}

	secret, ok := request.GetUser().Secrets[request.GetGitSecretID()]
	if !ok {
		err = fmt.Errorf("error getting secret '%s'", request.GetGitSecretID())
		logger.WithFields(log.Fields{"error": err}).Error("unable to update WorkflowDefinition without git Secret")
		return
	}

	logger.Info("updating WorkflowDefinition")
	err = wfd.edit(reqBytes, request.CommitMessage, secret)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to update WorkflowDefinition")
		return
	}
}

// Get synchronously responds with the WorkflowDefinition
func Get(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
	logger := log.WithFields(log.Fields{
		"package":  "workflowdefinition",
		"function": "Get",
	})

	var request Request
	err := json.Unmarshal(reqBytes, &request)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to unmarshal JSON bytes into Request")
		return
	}

	logRequest := request
	logRequest.BasicRequest = logRequest.GetRedacted()
	logger = logger.WithFields(log.Fields{"request": logRequest})

	logger.Info("getting WorkflowDefinition")
	wfd, err := get(request.GetWorkflowID(), request.GetUser().Username)
	var errResponse *common.Error
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to get WorkflowDefinition")
		errResponse = &common.Error{
			Message: fmt.Sprintf("unable to get WorkflowDefinition '%s': %v", request.GetWorkflowID(), err),
			Code:    500,
		}
	}

	// Marshal response
	data, err := json.Marshal(struct {
		Error              *common.Error       `json:"error,omitempty"`
		WorkflowDefinition *WorkflowDefinition `json:"data"`
	}{
		Error:              errResponse,
		WorkflowDefinition: wfd,
	})
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to marshal WorkflowDefinition response")
	}
	msg.Respond(data)
}

// GetForUser synchronously responds with all WorkflowDefinitions owned by a user
func GetForUser(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
	logger := log.WithFields(log.Fields{
		"package":  "workflowdefinition",
		"function": "GetForUser",
	})

	var request Request
	err := json.Unmarshal(reqBytes, &request)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to unmarshal JSON bytes into Request")
		return
	}

	logRequest := request
	logRequest.BasicRequest = logRequest.GetRedacted()
	logger = logger.WithFields(log.Fields{"request": logRequest})

	logger.Info("getting WorkflowDefinitions for user")
	wfds, err := getForUser(request.GetUser().Username)
	var errResponse *common.Error
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to get WorkflowDefinitions for user")
		errResponse = &common.Error{
			Message: fmt.Sprintf("unable to get WorkflowDefinitions for '%s': %v", request.GetUser().Username, err),
			Code:    500,
		}
	}

	// Marshal response
	data, jsonErr := json.Marshal(struct {
		Error               *common.Error        `json:"error,omitempty"`
		WorkflowDefinitions []WorkflowDefinition `json:"data"`
	}{
		Error:               errResponse,
		WorkflowDefinitions: wfds,
	})
	if jsonErr != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to marshal WorkflowDefinition response")
	}
	msg.Respond(data)
}
