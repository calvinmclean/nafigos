package common

import (
	"regexp"
	"testing"

	argo "github.com/argoproj/argo/pkg/apis/workflow/v1alpha1"
	"github.com/stretchr/testify/assert"
	apiauth "istio.io/api/authentication/v1alpha1"
	apinetworking "istio.io/api/networking/v1beta1"
	apisec "istio.io/api/security/v1beta1"
	auth "istio.io/client-go/pkg/apis/authentication/v1alpha1"
	istionetworking "istio.io/client-go/pkg/apis/networking/v1beta1"
	security "istio.io/client-go/pkg/apis/security/v1beta1"
	appsv1 "k8s.io/api/apps/v1"
	batchv1 "k8s.io/api/batch/v1"
	certificates "k8s.io/api/certificates/v1beta1"
	corev1 "k8s.io/api/core/v1"
	rbac "k8s.io/api/rbac/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"sigs.k8s.io/yaml"
)

func TestDNS1123NameValidUsernames(t *testing.T) {
	validUsernames := map[string]string{
		"test_user_123_":          "test-user-123",
		"123testuser":             "123testuser",
		"testuser":                "testuser",
		"test-user":               "test-user",
		"test-user-":              "test-user",
		"_test_user-":             "test-user",
		"!@#$%^!test_user#$@!%@-": "test-user",
	}

	for username, expected := range validUsernames {
		actual, err := GetDNS1123Name(username)
		assert.NoError(t, err)
		assert.Equal(t, expected, actual)
		assert.Regexp(t, regexp.MustCompile(`[a-z0-9]([-a-z0-9]*[a-z0-9])?`), actual)
	}
}

func TestDNS1123NameInvalidUsernames(t *testing.T) {
	invalidUsernames := []string{
		"!@#$%",
		"",
		" ~!@$%$@ ",
	}

	for _, v := range invalidUsernames {
		slug, err := GetDNS1123Name(v)
		assert.Equal(t, "", slug)
		assert.Error(t, err)
	}

}

func TestCanICreateDeploymentsFalse(t *testing.T) {
	clientsets, _ := NewTestK8sClientsets()
	c, err := clientsets.CanI("create", "deployments")
	assert.NoError(t, err)
	assert.False(t, c)
}

func TestCreateDeployment(t *testing.T) {
	name := "test"
	imageName := "testImage"
	clientsets, _ := NewTestK8sClientsets()
	testDeployment := &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:   name,
			Labels: map[string]string{"name": name},
		},
		Spec: appsv1.DeploymentSpec{
			Selector: &metav1.LabelSelector{
				MatchLabels: map[string]string{"name": name},
			},
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Name:   name,
					Labels: map[string]string{"name": name},
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{Name: name, Image: imageName},
					},
				},
			},
		},
	}
	_, err := clientsets.CreateDeployment(testDeployment, "default")
	assert.NoError(t, err)
}

func TestUpdateDeployment(t *testing.T) {
	name := "test"
	imageName := "testImage"
	clientsets, _ := NewTestK8sClientsets()
	testDeployment := &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:   name,
			Labels: map[string]string{"name": name},
		},
		Spec: appsv1.DeploymentSpec{
			Selector: &metav1.LabelSelector{
				MatchLabels: map[string]string{"name": name},
			},
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Name:   name,
					Labels: map[string]string{"name": name},
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{Name: name, Image: imageName},
					},
				},
			},
		},
	}
	_, err := clientsets.CreateDeployment(testDeployment, "default")
	assert.NoError(t, err)
	_, err = clientsets.UpdateDeployment(testDeployment, "default")
	assert.NoError(t, err)
}

func TestUpdateDeploymentErrorDeleting(t *testing.T) {
	name := "test"
	imageName := "testImage"
	clientsets, _ := NewTestK8sClientsets()
	testDeployment := &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:   name,
			Labels: map[string]string{"name": name},
		},
		Spec: appsv1.DeploymentSpec{
			Selector: &metav1.LabelSelector{
				MatchLabels: map[string]string{"name": name},
			},
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Name:   name,
					Labels: map[string]string{"name": name},
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{Name: name, Image: imageName},
					},
				},
			},
		},
	}
	_, err := clientsets.UpdateDeployment(testDeployment, "default")
	assert.Error(t, err)
}

func TestScaleDeployment(t *testing.T) {
	name := "test"
	imageName := "testImage"
	clientsets, _ := NewTestK8sClientsets()
	testDeployment := &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:   name,
			Labels: map[string]string{"name": name},
		},
		Spec: appsv1.DeploymentSpec{
			Selector: &metav1.LabelSelector{
				MatchLabels: map[string]string{"name": name},
			},
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Name:   name,
					Labels: map[string]string{"name": name},
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{Name: name, Image: imageName},
					},
				},
			},
		},
	}
	_, err := clientsets.CreateDeployment(testDeployment, "default")
	assert.NoError(t, err)
	err = clientsets.ScaleDeployment(name, "default", int32(3))
	assert.NoError(t, err)
}

func TestDeleteDeployment(t *testing.T) {
	name := "test"
	imageName := "testImage"
	clientsets, _ := NewTestK8sClientsets()
	testDeployment := &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:   name,
			Labels: map[string]string{"name": name},
		},
		Spec: appsv1.DeploymentSpec{
			Selector: &metav1.LabelSelector{
				MatchLabels: map[string]string{"name": name},
			},
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Name:   name,
					Labels: map[string]string{"name": name},
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{Name: name, Image: imageName},
					},
				},
			},
		},
	}
	_, err := clientsets.CreateDeployment(testDeployment, "default")
	assert.NoError(t, err)
	err = clientsets.DeleteDeployment(name, "default")
	assert.NoError(t, err)
}

func TestGetDeployment(t *testing.T) {
	name := "test"
	imageName := "testImage"
	clientsets, _ := NewTestK8sClientsets()
	testDeployment := &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:   name,
			Labels: map[string]string{"name": name},
		},
		Spec: appsv1.DeploymentSpec{
			Selector: &metav1.LabelSelector{
				MatchLabels: map[string]string{"name": name},
			},
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Name:   name,
					Labels: map[string]string{"name": name},
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{Name: name, Image: imageName},
					},
				},
			},
		},
	}
	_, err := clientsets.CreateDeployment(testDeployment, "default")
	assert.NoError(t, err)
	deployment, err := clientsets.GetDeployment(name, "default")
	assert.NoError(t, err)
	assert.Equal(t, name, deployment.Name)
}

func TestListDeployments(t *testing.T) {
	name := "test"
	imageName := "testImage"
	clientsets, _ := NewTestK8sClientsets()
	testDeployment := &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:   name,
			Labels: map[string]string{"name": name},
		},
		Spec: appsv1.DeploymentSpec{
			Selector: &metav1.LabelSelector{
				MatchLabels: map[string]string{"name": name},
			},
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Name:   name,
					Labels: map[string]string{"name": name},
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{Name: name, Image: imageName},
					},
				},
			},
		},
	}
	_, err := clientsets.CreateDeployment(testDeployment, "default")
	assert.NoError(t, err)

	deployments, err := clientsets.ListDeployments(metav1.ListOptions{}, "default")
	assert.NoError(t, err)
	assert.Equal(t, 1, len(deployments.Items))
}

func TestCreateService(t *testing.T) {
	name := "test"
	clientsets, _ := NewTestK8sClientsets()
	testService := &corev1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
			Labels: map[string]string{
				"app":     name,
				"service": name,
			},
		},
		Spec: corev1.ServiceSpec{
			Ports: []corev1.ServicePort{
				{Name: name, Port: int32(80)},
			},
			Selector: map[string]string{
				"app": name,
			},
		},
	}
	_, err := clientsets.CreateService(testService, "default")
	assert.NoError(t, err)
}

func TestUpdateService(t *testing.T) {
	name := "test"
	clientsets, _ := NewTestK8sClientsets()
	testService := &corev1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
			Labels: map[string]string{
				"app":     name,
				"service": name,
			},
		},
		Spec: corev1.ServiceSpec{
			Ports: []corev1.ServicePort{
				{Name: name, Port: int32(80)},
			},
			Selector: map[string]string{
				"app": name,
			},
		},
	}
	_, err := clientsets.CreateService(testService, "default")
	assert.NoError(t, err)
	_, err = clientsets.UpdateService(testService, "default")
	assert.NoError(t, err)
}

func TestUpdateServiceErrorDeleting(t *testing.T) {
	name := "test"
	clientsets, _ := NewTestK8sClientsets()
	testService := &corev1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
			Labels: map[string]string{
				"app":     name,
				"service": name,
			},
		},
		Spec: corev1.ServiceSpec{
			Ports: []corev1.ServicePort{
				{Name: name, Port: int32(80)},
			},
			Selector: map[string]string{
				"app": name,
			},
		},
	}
	_, err := clientsets.UpdateService(testService, "default")
	assert.Error(t, err)
}

func TestDeleteService(t *testing.T) {
	name := "test"
	clientsets, _ := NewTestK8sClientsets()
	testService := &corev1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
			Labels: map[string]string{
				"app":     name,
				"service": name,
			},
		},
		Spec: corev1.ServiceSpec{
			Ports: []corev1.ServicePort{
				{Name: name, Port: int32(80)},
			},
			Selector: map[string]string{
				"app": name,
			},
		},
	}
	_, err := clientsets.CreateService(testService, "default")
	assert.NoError(t, err)
	err = clientsets.DeleteService(name, "default")
	assert.NoError(t, err)
}

func TestGetService(t *testing.T) {
	name := "test"
	clientsets, _ := NewTestK8sClientsets()
	testService := &corev1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
			Labels: map[string]string{
				"app":     name,
				"service": name,
			},
		},
		Spec: corev1.ServiceSpec{
			Ports: []corev1.ServicePort{
				{Name: name, Port: int32(80)},
			},
			Selector: map[string]string{
				"app": name,
			},
		},
	}
	_, err := clientsets.CreateService(testService, "default")
	assert.NoError(t, err)
	service, err := clientsets.GetService(name, "default")
	assert.NoError(t, err)
	assert.Equal(t, name, service.Name)
}

func TestCreateScaledObject(t *testing.T) {
	name := "test"
	clientsets, _ := NewTestK8sClientsets()
	scaledObject := &unstructured.Unstructured{
		Object: map[string]interface{}{
			"apiVersion": "keda.k8s.io/v1alpha1",
			"kind":       "ScaledObject",
			"metadata": map[string]interface{}{
				"name":      name,
				"namespace": "default",
				"labels": map[string]interface{}{
					"deploymentName": name,
				},
			},
			"spec": map[string]interface{}{
				"scaleTargetRef": map[string]interface{}{
					"deploymentName": name,
				},
				"pollingInterval": "5",
				"cooldownPeriod":  "40",
				"maxReplicaCount": "30",
				"triggers": []interface{}{
					map[string]interface{}{
						"type": "rabbitmq",
						"metadata": map[string]interface{}{
							"queueName":   "hello",
							"host":        "RabbitMqHost",
							"queueLength": "5",
						},
					},
				},
			},
		},
	}
	_, err := clientsets.CreateScaledObject(scaledObject, "default")
	assert.NoError(t, err)
}

func TestUpdateScaledObject(t *testing.T) {
	name := "test"
	clientsets, _ := NewTestK8sClientsets()
	scaledObject := &unstructured.Unstructured{
		Object: map[string]interface{}{
			"apiVersion": "keda.k8s.io/v1alpha1",
			"kind":       "ScaledObject",
			"metadata": map[string]interface{}{
				"name":      name,
				"namespace": "default",
				"labels": map[string]interface{}{
					"deploymentName": name,
				},
			},
			"spec": map[string]interface{}{
				"scaleTargetRef": map[string]interface{}{
					"deploymentName": name,
				},
				"pollingInterval": "5",
				"cooldownPeriod":  "40",
				"maxReplicaCount": "30",
				"triggers": []interface{}{
					map[string]interface{}{
						"type": "rabbitmq",
						"metadata": map[string]interface{}{
							"queueName":   "hello",
							"host":        "RabbitMqHost",
							"queueLength": "5",
						},
					},
				},
			},
		},
	}
	_, err := clientsets.CreateScaledObject(scaledObject, "default")
	assert.NoError(t, err)
	_, err = clientsets.UpdateScaledObject(scaledObject, "default")
	assert.NoError(t, err)
}

func TestUpdateScaledObjectErrorDeleting(t *testing.T) {
	name := "test"
	clientsets, _ := NewTestK8sClientsets()
	scaledObject := &unstructured.Unstructured{
		Object: map[string]interface{}{
			"apiVersion": "keda.k8s.io/v1alpha1",
			"kind":       "ScaledObject",
			"metadata": map[string]interface{}{
				"name":      name,
				"namespace": "default",
				"labels": map[string]interface{}{
					"deploymentName": name,
				},
			},
			"spec": map[string]interface{}{
				"scaleTargetRef": map[string]interface{}{
					"deploymentName": name,
				},
				"pollingInterval": "5",
				"cooldownPeriod":  "40",
				"maxReplicaCount": "30",
				"triggers": []interface{}{
					map[string]interface{}{
						"type": "rabbitmq",
						"metadata": map[string]interface{}{
							"queueName":   "hello",
							"host":        "RabbitMqHost",
							"queueLength": "5",
						},
					},
				},
			},
		},
	}
	_, err := clientsets.UpdateScaledObject(scaledObject, "default")
	assert.Error(t, err)
}

func TestDeleteScaledObject(t *testing.T) {
	name := "test"
	clientsets, _ := NewTestK8sClientsets()
	scaledObject := &unstructured.Unstructured{
		Object: map[string]interface{}{
			"apiVersion": "keda.k8s.io/v1alpha1",
			"kind":       "ScaledObject",
			"metadata": map[string]interface{}{
				"name":      name,
				"namespace": "default",
				"labels": map[string]interface{}{
					"deploymentName": name,
				},
			},
			"spec": map[string]interface{}{
				"scaleTargetRef": map[string]interface{}{
					"deploymentName": name,
				},
				"pollingInterval": "5",
				"cooldownPeriod":  "40",
				"maxReplicaCount": "30",
				"triggers": []interface{}{
					map[string]interface{}{
						"type": "rabbitmq",
						"metadata": map[string]interface{}{
							"queueName":   "hello",
							"host":        "RabbitMqHost",
							"queueLength": "5",
						},
					},
				},
			},
		},
	}
	_, err := clientsets.CreateScaledObject(scaledObject, "default")
	assert.NoError(t, err)
	err = clientsets.DeleteScaledObject(name, "default")
	assert.NoError(t, err)
}

func TestCreateArgoWorkflow(t *testing.T) {
	var workflow *argo.Workflow
	wfd := []byte(`
apiVersion: argoproj.io/v1alpha1
kind: Workflow
metadata:
  generateName: nafigos-say-
spec:
  entrypoint: nafigos-say
  templates:
  - name: nafigos-say
`)
	err := yaml.Unmarshal(wfd, &workflow)
	assert.NoError(t, err)

	clientsets, _ := NewTestK8sClientsets()
	_, err = clientsets.CreateArgoWorkflow(workflow, "default")
	assert.NoError(t, err)
}

func TestUpdateArgoWorkflow(t *testing.T) {
	var workflow *argo.Workflow
	wfd := []byte(`
apiVersion: argoproj.io/v1alpha1
kind: Workflow
metadata:
  generateName: nafigos-say-
spec:
  entrypoint: nafigos-say
  templates:
  - name: nafigos-say
`)
	err := yaml.Unmarshal(wfd, &workflow)
	assert.NoError(t, err)

	clientsets, _ := NewTestK8sClientsets()
	_, err = clientsets.CreateArgoWorkflow(workflow, "default")
	assert.NoError(t, err)
	_, err = clientsets.UpdateArgoWorkflow(workflow, "default")
	assert.NoError(t, err)
}

func TestUpdateArgoWorkflowErrorDeleting(t *testing.T) {
	var workflow *argo.Workflow
	wfd := []byte(`
apiVersion: argoproj.io/v1alpha1
kind: Workflow
metadata:
  generateName: nafigos-say-
spec:
  entrypoint: nafigos-say
  templates:
  - name: nafigos-say
`)
	err := yaml.Unmarshal(wfd, &workflow)
	assert.NoError(t, err)

	clientsets, _ := NewTestK8sClientsets()
	_, err = clientsets.UpdateArgoWorkflow(workflow, "default")
	assert.Error(t, err)
}

func TestDeleteArgoWorkflow(t *testing.T) {
	var workflow *argo.Workflow
	wfd := []byte(`
apiVersion: argoproj.io/v1alpha1
kind: Workflow
metadata:
  generateName: nafigos-say-
  name: nafigos-say
spec:
  entrypoint: nafigos-say
  templates:
  - name: nafigos-say
`)
	err := yaml.Unmarshal(wfd, &workflow)
	assert.NoError(t, err)

	clientsets, _ := NewTestK8sClientsets()
	_, err = clientsets.CreateArgoWorkflow(workflow, "default")
	assert.NoError(t, err)
	err = clientsets.DeleteArgoWorkflow("nafigos-say", "default")
	assert.NoError(t, err)
}

func TestGetArgoWorkflow(t *testing.T) {
	var workflow *argo.Workflow
	wfd := []byte(`
apiVersion: argoproj.io/v1alpha1
kind: Workflow
metadata:
  generateName: nafigos-say-
  name: nafigos-say
spec:
  entrypoint: nafigos-say
  templates:
  - name: nafigos-say
`)
	err := yaml.Unmarshal(wfd, &workflow)
	assert.NoError(t, err)

	clientsets, _ := NewTestK8sClientsets()
	_, err = clientsets.CreateArgoWorkflow(workflow, "default")
	assert.NoError(t, err)
	wf, err := clientsets.GetArgoWorkflow("nafigos-say", "default")
	assert.NoError(t, err)
	assert.Equal(t, "nafigos-say", wf.Name)
}

func TestListArgoWorkflows(t *testing.T) {
	var workflow *argo.Workflow
	wfd := []byte(`
apiVersion: argoproj.io/v1alpha1
kind: Workflow
metadata:
  generateName: nafigos-say-
  name: nafigos-say
spec:
  entrypoint: nafigos-say
  templates:
  - name: nafigos-say
`)
	err := yaml.Unmarshal(wfd, &workflow)
	assert.NoError(t, err)

	clientsets, _ := NewTestK8sClientsets()
	_, err = clientsets.CreateArgoWorkflow(workflow, "default")
	assert.NoError(t, err)
	wfs, err := clientsets.ListArgoWorkflows(metav1.ListOptions{}, "default")
	assert.NoError(t, err)
	assert.Equal(t, 1, len(wfs.Items))
}

func TestCreateSecret(t *testing.T) {
	clientsets, _ := NewTestK8sClientsets()
	secret := &corev1.Secret{
		TypeMeta:   metav1.TypeMeta{Kind: "Secret", APIVersion: "v1"},
		ObjectMeta: metav1.ObjectMeta{Name: "test"},
		StringData: map[string]string{"foo": "barsecret"},
		Type:       corev1.SecretTypeOpaque,
	}
	_, err := clientsets.CreateSecret(secret, "default")
	assert.NoError(t, err)
}

func TestUpdateSecret(t *testing.T) {
	clientsets, _ := NewTestK8sClientsets()
	secret := &corev1.Secret{
		TypeMeta:   metav1.TypeMeta{Kind: "Secret", APIVersion: "v1"},
		ObjectMeta: metav1.ObjectMeta{Name: "test"},
		StringData: map[string]string{"foo": "barsecret"},
		Type:       corev1.SecretTypeOpaque,
	}
	secret, err := clientsets.CreateSecret(secret, "default")
	assert.NoError(t, err)
	secret.StringData["foo"] = "bazsecret"
	secret, err = clientsets.UpdateSecret(secret, "default")
	assert.NoError(t, err)
}

func TestUpdateSecretErrorDeleting(t *testing.T) {
	clientsets, _ := NewTestK8sClientsets()
	secret := &corev1.Secret{
		TypeMeta:   metav1.TypeMeta{Kind: "Secret", APIVersion: "v1"},
		ObjectMeta: metav1.ObjectMeta{Name: "test"},
		StringData: map[string]string{"foo": "barsecret"},
		Type:       corev1.SecretTypeOpaque,
	}
	_, err := clientsets.UpdateSecret(secret, "default")
	assert.Error(t, err)
}

func TestDeleteSecret(t *testing.T) {
	clientsets, _ := NewTestK8sClientsets()
	secret := &corev1.Secret{
		TypeMeta:   metav1.TypeMeta{Kind: "Secret", APIVersion: "v1"},
		ObjectMeta: metav1.ObjectMeta{Name: "test"},
		StringData: map[string]string{"foo": "barsecret"},
		Type:       corev1.SecretTypeOpaque,
	}
	_, err := clientsets.CreateSecret(secret, "default")
	assert.NoError(t, err)
	err = clientsets.DeleteSecret("test", "default")
	assert.NoError(t, err)
}

func TestCreatePods(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	pod := &corev1.Pod{
		TypeMeta:   metav1.TypeMeta{Kind: "Pod", APIVersion: "v1"},
		ObjectMeta: metav1.ObjectMeta{Name: "Test Pod"},
		Spec: corev1.PodSpec{
			Containers: []corev1.Container{
				{
					Name:  "kaniko",
					Image: "gcr.io/kaniko-project/executor:latest",
				},
			},
			RestartPolicy: corev1.RestartPolicyNever,
		},
	}
	_, err = clientsets.CreatePods(pod, "default")
	assert.NoError(t, err)
}

func TestGetPods(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	pod := &corev1.Pod{
		TypeMeta:   metav1.TypeMeta{Kind: "Pod", APIVersion: "v1"},
		ObjectMeta: metav1.ObjectMeta{Name: "Test Pod"},
		Spec: corev1.PodSpec{
			Containers: []corev1.Container{
				{
					Name:  "kaniko",
					Image: "gcr.io/kaniko-project/executor:latest",
				},
			},
			RestartPolicy: corev1.RestartPolicyNever,
		},
	}
	_, err = clientsets.CreatePods(pod, "default")
	assert.NoError(t, err)
	_, err = clientsets.GetPods("Test Pod", "default")
	assert.NoError(t, err)
}

func TestGetPodsNotFound(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	_, err = clientsets.GetPods("Test Pod", "default")
	assert.Error(t, err)
}

func TestListPods(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)

	pod := &corev1.Pod{
		TypeMeta:   metav1.TypeMeta{Kind: "Pod", APIVersion: "v1"},
		ObjectMeta: metav1.ObjectMeta{Name: "Test Pod"},
		Spec: corev1.PodSpec{
			Containers: []corev1.Container{
				{
					Name:  "kaniko",
					Image: "gcr.io/kaniko-project/executor:latest",
				},
			},
			RestartPolicy: corev1.RestartPolicyNever,
		},
	}
	_, err = clientsets.CreatePods(pod, "default")
	assert.NoError(t, err)

	pods, err := clientsets.ListPods(metav1.ListOptions{}, "default")
	assert.NoError(t, err)
	assert.Equal(t, 1, len(pods.Items))
}

func TestListPodsEmpty(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)

	pods, err := clientsets.ListPods(metav1.ListOptions{}, "default")
	assert.NoError(t, err)
	assert.Empty(t, pods.Items)
}

func TestDeletePods(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	err = clientsets.DeletePods("Test Pod", "default")
	assert.Error(t, err)
}

func TestCreateJob(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	name := "test-job"
	job := &batchv1.Job{
		ObjectMeta: metav1.ObjectMeta{Name: name},
		Spec: batchv1.JobSpec{
			Template: corev1.PodTemplateSpec{
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{Name: name, Image: "testImage"},
					},
					RestartPolicy: corev1.RestartPolicyOnFailure,
				},
			},
		},
	}
	_, err = clientsets.CreateJob(job, "default")
	assert.NoError(t, err)
}

func TestGetJob(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	name := "test-job"
	job := &batchv1.Job{
		ObjectMeta: metav1.ObjectMeta{Name: name},
		Spec: batchv1.JobSpec{
			Template: corev1.PodTemplateSpec{
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{Name: name, Image: "testImage"},
					},
					RestartPolicy: corev1.RestartPolicyOnFailure,
				},
			},
		},
	}
	_, err = clientsets.CreateJob(job, "default")
	assert.NoError(t, err)
	job, err = clientsets.GetJob(name, "default")
	assert.NoError(t, err)
	assert.Equal(t, name, job.Name)
}

func TestListJobs(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	name := "test-job"
	job := &batchv1.Job{
		ObjectMeta: metav1.ObjectMeta{Name: name},
		Spec: batchv1.JobSpec{
			Template: corev1.PodTemplateSpec{
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{Name: name, Image: "testImage"},
					},
					RestartPolicy: corev1.RestartPolicyOnFailure,
				},
			},
		},
	}
	_, err = clientsets.CreateJob(job, "default")
	assert.NoError(t, err)
	jobs, err := clientsets.ListJobs(metav1.ListOptions{}, "default")
	assert.NoError(t, err)
	assert.Equal(t, 1, len(jobs.Items))
}

func TestDeleteJob(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	name := "test-job"
	job := &batchv1.Job{
		ObjectMeta: metav1.ObjectMeta{Name: name},
		Spec: batchv1.JobSpec{
			Template: corev1.PodTemplateSpec{
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{Name: name, Image: "testImage"},
					},
					RestartPolicy: corev1.RestartPolicyOnFailure,
				},
			},
		},
	}
	_, err = clientsets.CreateJob(job, "default")
	assert.NoError(t, err)
	err = clientsets.DeleteJob(name, "default")
	assert.NoError(t, err)
}

func TestUpdateJob(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	name := "test-job"
	job := &batchv1.Job{
		ObjectMeta: metav1.ObjectMeta{Name: name},
		Spec: batchv1.JobSpec{
			Template: corev1.PodTemplateSpec{
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{Name: name, Image: "testImage"},
					},
					RestartPolicy: corev1.RestartPolicyOnFailure,
				},
			},
		},
	}
	_, err = clientsets.CreateJob(job, "default")
	assert.NoError(t, err)
	_, err = clientsets.UpdateJob(job, "default")
	assert.NoError(t, err)
}

func TestUpdateJobErrorDeleting(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	name := "test-job"
	job := &batchv1.Job{
		ObjectMeta: metav1.ObjectMeta{Name: name},
		Spec: batchv1.JobSpec{
			Template: corev1.PodTemplateSpec{
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{Name: name, Image: "testImage"},
					},
					RestartPolicy: corev1.RestartPolicyOnFailure,
				},
			},
		},
	}
	_, err = clientsets.UpdateJob(job, "default")
	assert.Error(t, err)
}

func TestCreateDestinationRule(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	name := "test-dr"
	dr := &istionetworking.DestinationRule{
		ObjectMeta: metav1.ObjectMeta{
			Name:   name,
			Labels: map[string]string{"app": name},
		},
		Spec: apinetworking.DestinationRule{
			Host: name,
			Subsets: []*apinetworking.Subset{
				{
					Name:   "v1",
					Labels: map[string]string{"app": name},
				},
			},
		},
	}
	_, err = clientsets.CreateDestinationRule(dr, "default")
	assert.NoError(t, err)
}

func TestUpdateDestinationRule(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	name := "test-dr"
	dr := &istionetworking.DestinationRule{
		ObjectMeta: metav1.ObjectMeta{
			Name:   name,
			Labels: map[string]string{"app": name},
		},
		Spec: apinetworking.DestinationRule{
			Host: name,
			Subsets: []*apinetworking.Subset{
				{
					Name:   "v1",
					Labels: map[string]string{"app": name},
				},
			},
		},
	}
	_, err = clientsets.CreateDestinationRule(dr, "default")
	assert.NoError(t, err)
	_, err = clientsets.UpdateDestinationRule(dr, "default")
	assert.NoError(t, err)
}

func TestUpdateDestinationRuleErrorDeleting(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	name := "test-dr"
	dr := &istionetworking.DestinationRule{
		ObjectMeta: metav1.ObjectMeta{
			Name:   name,
			Labels: map[string]string{"app": name},
		},
		Spec: apinetworking.DestinationRule{
			Host: name,
			Subsets: []*apinetworking.Subset{
				{
					Name:   "v1",
					Labels: map[string]string{"app": name},
				},
			},
		},
	}
	_, err = clientsets.UpdateDestinationRule(dr, "default")
	assert.Error(t, err)
}

func TestDeleteDestinationRule(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	name := "test-dr"
	dr := &istionetworking.DestinationRule{
		ObjectMeta: metav1.ObjectMeta{
			Name:   name,
			Labels: map[string]string{"app": name},
		},
		Spec: apinetworking.DestinationRule{
			Host: name,
			Subsets: []*apinetworking.Subset{
				{
					Name:   "v1",
					Labels: map[string]string{"app": name},
				},
			},
		},
	}
	_, err = clientsets.CreateDestinationRule(dr, "default")
	assert.NoError(t, err)
	err = clientsets.DeleteDestinationRule(name, "default")
	assert.NoError(t, err)
}

func TestCreateVirtualService(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	name := "test-vs"
	vs := &istionetworking.VirtualService{
		ObjectMeta: metav1.ObjectMeta{
			Name:   name,
			Labels: map[string]string{"app": name},
		},
		Spec: apinetworking.VirtualService{
			Gateways: []string{"nafigos-gateway", name},
			Hosts:    []string{"host"},
			Http: []*apinetworking.HTTPRoute{
				{
					Match: []*apinetworking.HTTPMatchRequest{
						{
							Uri: &apinetworking.StringMatch{
								MatchType: &apinetworking.StringMatch_Prefix{
									Prefix: "/",
								},
							},
						},
						{
							Uri: &apinetworking.StringMatch{
								MatchType: &apinetworking.StringMatch_Prefix{
									Prefix: "/",
								},
							},
						},
					},
					Rewrite: &apinetworking.HTTPRewrite{Uri: "/"},
					Route: []*apinetworking.HTTPRouteDestination{
						{
							Destination: &apinetworking.Destination{
								Host:   name,
								Subset: "v1",
								Port:   &apinetworking.PortSelector{Number: uint32(80)},
							},
						},
					},
				},
			},
		},
	}
	_, err = clientsets.CreateVirtualService(vs, "default")
	assert.NoError(t, err)
}

func TestUpdateVirtualService(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	name := "test-vs"
	vs := &istionetworking.VirtualService{
		ObjectMeta: metav1.ObjectMeta{
			Name:   name,
			Labels: map[string]string{"app": name},
		},
		Spec: apinetworking.VirtualService{
			Gateways: []string{"nafigos-gateway", name},
			Hosts:    []string{"host"},
			Http: []*apinetworking.HTTPRoute{
				{
					Match: []*apinetworking.HTTPMatchRequest{
						{
							Uri: &apinetworking.StringMatch{
								MatchType: &apinetworking.StringMatch_Prefix{
									Prefix: "/",
								},
							},
						},
						{
							Uri: &apinetworking.StringMatch{
								MatchType: &apinetworking.StringMatch_Prefix{
									Prefix: "/",
								},
							},
						},
					},
					Rewrite: &apinetworking.HTTPRewrite{Uri: "/"},
					Route: []*apinetworking.HTTPRouteDestination{
						{
							Destination: &apinetworking.Destination{
								Host:   name,
								Subset: "v1",
								Port:   &apinetworking.PortSelector{Number: uint32(80)},
							},
						},
					},
				},
			},
		},
	}
	_, err = clientsets.CreateVirtualService(vs, "default")
	assert.NoError(t, err)
	_, err = clientsets.UpdateVirtualService(vs, "default")
	assert.NoError(t, err)
}

func TestUpdateVirtualServiceErrorDeleting(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	name := "test-vs"
	vs := &istionetworking.VirtualService{
		ObjectMeta: metav1.ObjectMeta{
			Name:   name,
			Labels: map[string]string{"app": name},
		},
		Spec: apinetworking.VirtualService{
			Gateways: []string{"nafigos-gateway", name},
			Hosts:    []string{"host"},
			Http: []*apinetworking.HTTPRoute{
				{
					Match: []*apinetworking.HTTPMatchRequest{
						{
							Uri: &apinetworking.StringMatch{
								MatchType: &apinetworking.StringMatch_Prefix{
									Prefix: "/",
								},
							},
						},
						{
							Uri: &apinetworking.StringMatch{
								MatchType: &apinetworking.StringMatch_Prefix{
									Prefix: "/",
								},
							},
						},
					},
					Rewrite: &apinetworking.HTTPRewrite{Uri: "/"},
					Route: []*apinetworking.HTTPRouteDestination{
						{
							Destination: &apinetworking.Destination{
								Host:   name,
								Subset: "v1",
								Port:   &apinetworking.PortSelector{Number: uint32(80)},
							},
						},
					},
				},
			},
		},
	}
	_, err = clientsets.UpdateVirtualService(vs, "default")
	assert.Error(t, err)
}

func TestDeleteVirtualService(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	name := "test-vs"
	vs := &istionetworking.VirtualService{
		ObjectMeta: metav1.ObjectMeta{
			Name:   name,
			Labels: map[string]string{"app": name},
		},
		Spec: apinetworking.VirtualService{
			Gateways: []string{"nafigos-gateway", name},
			Hosts:    []string{"host"},
			Http: []*apinetworking.HTTPRoute{
				{
					Match: []*apinetworking.HTTPMatchRequest{
						{
							Uri: &apinetworking.StringMatch{
								MatchType: &apinetworking.StringMatch_Prefix{
									Prefix: "/",
								},
							},
						},
						{
							Uri: &apinetworking.StringMatch{
								MatchType: &apinetworking.StringMatch_Prefix{
									Prefix: "/",
								},
							},
						},
					},
					Rewrite: &apinetworking.HTTPRewrite{Uri: "/"},
					Route: []*apinetworking.HTTPRouteDestination{
						{
							Destination: &apinetworking.Destination{
								Host:   name,
								Subset: "v1",
								Port:   &apinetworking.PortSelector{Number: uint32(80)},
							},
						},
					},
				},
			},
		},
	}
	_, err = clientsets.CreateVirtualService(vs, "default")
	assert.NoError(t, err)
	err = clientsets.DeleteVirtualService(name, "default")
	assert.NoError(t, err)
}

func TestCreateGateway(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	name := "test-gw"
	gw := &istionetworking.Gateway{
		ObjectMeta: metav1.ObjectMeta{Name: name},
		Spec: apinetworking.Gateway{
			Selector: map[string]string{"istio": "ingressgateway"},
			Servers: []*apinetworking.Server{
				{
					Hosts: []string{"*"},
					Port: &apinetworking.Port{
						Number:   uint32(80),
						Name:     "http",
						Protocol: "HTTP",
					},
				},
			},
		},
	}
	_, err = clientsets.CreateGateway(gw, "default")
	assert.NoError(t, err)
}

func TestUpdateGateway(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	name := "test-gw"
	gw := &istionetworking.Gateway{
		ObjectMeta: metav1.ObjectMeta{Name: name},
		Spec: apinetworking.Gateway{
			Selector: map[string]string{"istio": "ingressgateway"},
			Servers: []*apinetworking.Server{
				{
					Hosts: []string{"*"},
					Port: &apinetworking.Port{
						Number:   uint32(80),
						Name:     "http",
						Protocol: "HTTP",
					},
				},
			},
		},
	}
	_, err = clientsets.CreateGateway(gw, "default")
	assert.NoError(t, err)
	_, err = clientsets.UpdateGateway(gw, "default")
	assert.NoError(t, err)
}

func TestUpdateGatewayErrorDeleting(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	name := "test-gw"
	gw := &istionetworking.Gateway{
		ObjectMeta: metav1.ObjectMeta{Name: name},
		Spec: apinetworking.Gateway{
			Selector: map[string]string{"istio": "ingressgateway"},
			Servers: []*apinetworking.Server{
				{
					Hosts: []string{"*"},
					Port: &apinetworking.Port{
						Number:   uint32(80),
						Name:     "http",
						Protocol: "HTTP",
					},
				},
			},
		},
	}
	_, err = clientsets.UpdateGateway(gw, "default")
	assert.Error(t, err)
}

func TestDeleteGateway(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	name := "test-gw"
	gw := &istionetworking.Gateway{
		ObjectMeta: metav1.ObjectMeta{Name: name},
		Spec: apinetworking.Gateway{
			Selector: map[string]string{"istio": "ingressgateway"},
			Servers: []*apinetworking.Server{
				{
					Hosts: []string{"*"},
					Port: &apinetworking.Port{
						Number:   uint32(80),
						Name:     "http",
						Protocol: "HTTP",
					},
				},
			},
		},
	}
	_, err = clientsets.CreateGateway(gw, "default")
	assert.NoError(t, err)
	err = clientsets.DeleteGateway(name, "default")
	assert.NoError(t, err)
}

func TestCreateServiceEntry(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	name := "test-se"
	se := &istionetworking.ServiceEntry{
		ObjectMeta: metav1.ObjectMeta{Name: name},
		Spec: apinetworking.ServiceEntry{
			Hosts:     []string{"host"},
			Addresses: []string{"address/32"},
			Ports: []*apinetworking.Port{
				{
					Number:   uint32(80),
					Name:     "tls",
					Protocol: "TLS",
				},
			},
			Location:   apinetworking.ServiceEntry_MESH_EXTERNAL,
			Resolution: apinetworking.ServiceEntry_STATIC,
			Endpoints:  []*apinetworking.WorkloadEntry{{Address: "address"}},
		},
	}
	_, err = clientsets.CreateServiceEntry(se, "default")
	assert.NoError(t, err)
}

func TestDeleteServiceEntry(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	name := "test-se"
	se := &istionetworking.ServiceEntry{
		ObjectMeta: metav1.ObjectMeta{Name: name},
		Spec: apinetworking.ServiceEntry{
			Hosts:     []string{"host"},
			Addresses: []string{"address/32"},
			Ports: []*apinetworking.Port{
				{
					Number:   uint32(80),
					Name:     "tls",
					Protocol: "TLS",
				},
			},
			Location:   apinetworking.ServiceEntry_MESH_EXTERNAL,
			Resolution: apinetworking.ServiceEntry_STATIC,
			Endpoints:  []*apinetworking.WorkloadEntry{{Address: "address"}},
		},
	}
	_, err = clientsets.CreateServiceEntry(se, "default")
	assert.NoError(t, err)
	err = clientsets.DeleteServiceEntry(name, "default")
	assert.NoError(t, err)
}

func TestUpdateServiceEntry(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	name := "test-se"
	se := &istionetworking.ServiceEntry{
		ObjectMeta: metav1.ObjectMeta{Name: name},
		Spec: apinetworking.ServiceEntry{
			Hosts:     []string{"host"},
			Addresses: []string{"address/32"},
			Ports: []*apinetworking.Port{
				{
					Number:   uint32(80),
					Name:     "tls",
					Protocol: "TLS",
				},
			},
			Location:   apinetworking.ServiceEntry_MESH_EXTERNAL,
			Resolution: apinetworking.ServiceEntry_STATIC,
			Endpoints:  []*apinetworking.WorkloadEntry{{Address: "address"}},
		},
	}
	_, err = clientsets.CreateServiceEntry(se, "default")
	assert.NoError(t, err)
	_, err = clientsets.UpdateServiceEntry(se, "default")
	assert.NoError(t, err)
}

func TestUpdateServiceEntryErrorDeleting(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	name := "test-se"
	se := &istionetworking.ServiceEntry{
		ObjectMeta: metav1.ObjectMeta{Name: name},
		Spec: apinetworking.ServiceEntry{
			Hosts:     []string{"host"},
			Addresses: []string{"address/32"},
			Ports: []*apinetworking.Port{
				{
					Number:   uint32(80),
					Name:     "tls",
					Protocol: "TLS",
				},
			},
			Location:   apinetworking.ServiceEntry_MESH_EXTERNAL,
			Resolution: apinetworking.ServiceEntry_STATIC,
			Endpoints:  []*apinetworking.WorkloadEntry{{Address: "address"}},
		},
	}
	_, err = clientsets.UpdateServiceEntry(se, "default")
	assert.Error(t, err)
}

func TestCreateAuthorizationPolicy(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	name := "test-ap"
	ap := &security.AuthorizationPolicy{
		ObjectMeta: metav1.ObjectMeta{Name: name},
		Spec: apisec.AuthorizationPolicy{
			Rules: []*apisec.Rule{
				{
					To: []*apisec.Rule_To{
						{
							Operation: &apisec.Operation{
								Paths: []string{"/*"},
								Hosts: []string{"host"},
							},
						},
					},
					When: []*apisec.Condition{
						{
							Key:    "request.auth.claims[preferred_username]",
							Values: []string{name},
						},
					},
				},
			},
		},
	}
	_, err = clientsets.CreateAuthorizationPolicy(ap, "default")
	assert.NoError(t, err)
}

func TestDeleteAuthorizationPolicy(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	name := "test-ap"
	ap := &security.AuthorizationPolicy{
		ObjectMeta: metav1.ObjectMeta{Name: name},
		Spec: apisec.AuthorizationPolicy{
			Rules: []*apisec.Rule{
				{
					To: []*apisec.Rule_To{
						{
							Operation: &apisec.Operation{
								Paths: []string{"/*"},
								Hosts: []string{"host"},
							},
						},
					},
					When: []*apisec.Condition{
						{
							Key:    "request.auth.claims[preferred_username]",
							Values: []string{name},
						},
					},
				},
			},
		},
	}
	_, err = clientsets.CreateAuthorizationPolicy(ap, "default")
	assert.NoError(t, err)
	err = clientsets.DeleteAuthorizationPolicy(name, "default")
	assert.NoError(t, err)
}

func TestCreatePolicy(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	p := &auth.Policy{
		ObjectMeta: metav1.ObjectMeta{Name: "default"},
		Spec: apiauth.Policy{
			Origins: []*apiauth.OriginAuthenticationMethod{
				{
					Jwt: &apiauth.Jwt{
						Issuer:     "keycloakURL",
						JwtHeaders: []string{"Authorization"},
						Audiences:  []string{"nafigos-client"},
						JwksUri:    "keycloakURL/protocol/openid-connect/certs",
					},
				},
			},
			PrincipalBinding: apiauth.PrincipalBinding_USE_ORIGIN,
		},
	}
	_, err = clientsets.CreatePolicy(p, "default")
	assert.NoError(t, err)
}

func TestDeletePolicy(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	p := &auth.Policy{
		ObjectMeta: metav1.ObjectMeta{Name: "default"},
		Spec: apiauth.Policy{
			Origins: []*apiauth.OriginAuthenticationMethod{
				{
					Jwt: &apiauth.Jwt{
						Issuer:     "keycloakURL",
						JwtHeaders: []string{"Authorization"},
						Audiences:  []string{"nafigos-client"},
						JwksUri:    "keycloakURL/protocol/openid-connect/certs",
					},
				},
			},
			PrincipalBinding: apiauth.PrincipalBinding_USE_ORIGIN,
		},
	}
	_, err = clientsets.CreatePolicy(p, "default")
	assert.NoError(t, err)
	err = clientsets.DeletePolicy("default", "default")
	assert.NoError(t, err)
}

func TestCreateNamespace(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	_, err = clientsets.CreateNamespace(&corev1.Namespace{ObjectMeta: metav1.ObjectMeta{Name: "namespace"}})
	assert.NoError(t, err)
}

func TestCreateRole(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	role := &rbac.Role{
		ObjectMeta: metav1.ObjectMeta{Name: "test-pod-reader"},
		Rules: []rbac.PolicyRule{
			{
				APIGroups: []string{""},
				Resources: []string{"pods", "pods/log"},
				Verbs:     []string{"get", "list", "watch"},
			},
		},
	}
	_, err = clientsets.CreateRole(role, "default")
	assert.NoError(t, err)
}

func TestCreateRoleBinding(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	rb := &rbac.RoleBinding{
		ObjectMeta: metav1.ObjectMeta{Name: "test"},
		Subjects: []rbac.Subject{
			{
				Kind:      "ServiceAccount",
				Name:      "default",
				Namespace: "default",
			},
		},
		RoleRef: rbac.RoleRef{
			APIGroup: "rbac.authorization.k8s.io",
			Kind:     "Role",
			Name:     "test",
		},
	}
	_, err = clientsets.CreateRoleBinding(rb, "default")
	assert.NoError(t, err)
}

func TestCreateCSR(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	csr := &certificates.CertificateSigningRequest{
		ObjectMeta: metav1.ObjectMeta{Name: "test"},
		Spec: certificates.CertificateSigningRequestSpec{
			Request: []byte(""),
			Groups:  []string{"system:authenticated"},
			Usages:  []certificates.KeyUsage{certificates.UsageClientAuth},
		},
	}
	_, err = clientsets.CreateCertificateSigningRequest(csr)
	assert.NoError(t, err)
}

func TestApproveCSR(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	csr := &certificates.CertificateSigningRequest{
		ObjectMeta: metav1.ObjectMeta{Name: "test"},
		Spec: certificates.CertificateSigningRequestSpec{
			Request: []byte(""),
			Groups:  []string{"system:authenticated"},
			Usages:  []certificates.KeyUsage{certificates.UsageClientAuth},
		},
	}
	approval := &certificates.CertificateSigningRequest{
		ObjectMeta: metav1.ObjectMeta{Name: "test"},
		Status: certificates.CertificateSigningRequestStatus{
			Conditions: []certificates.CertificateSigningRequestCondition{
				{
					Type:   certificates.CertificateApproved,
					Reason: "ApprovedByMyPolicy",
				},
			},
		},
	}
	_, err = clientsets.CreateCertificateSigningRequest(csr)
	assert.NoError(t, err)
	_, err = clientsets.ApproveCertificateSigningRequest(approval)
	assert.NoError(t, err)
}

func TestGetCertificateSigningRequest(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	csr := &certificates.CertificateSigningRequest{
		ObjectMeta: metav1.ObjectMeta{Name: "test"},
		Spec: certificates.CertificateSigningRequestSpec{
			Request: []byte(""),
			Groups:  []string{"system:authenticated"},
			Usages:  []certificates.KeyUsage{certificates.UsageClientAuth},
		},
	}
	_, err = clientsets.CreateCertificateSigningRequest(csr)
	assert.NoError(t, err)
	csrGet, err := clientsets.GetCertificateSigningRequest("test")
	assert.NoError(t, err)
	assert.Equal(t, "test", csrGet.Name)
}

func TestDeleteCertificateSigningRequest(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	csr := &certificates.CertificateSigningRequest{
		ObjectMeta: metav1.ObjectMeta{Name: "test"},
		Spec: certificates.CertificateSigningRequestSpec{
			Request: []byte(""),
			Groups:  []string{"system:authenticated"},
			Usages:  []certificates.KeyUsage{certificates.UsageClientAuth},
		},
	}
	_, err = clientsets.CreateCertificateSigningRequest(csr)
	assert.NoError(t, err)
	err = clientsets.DeleteCertificateSigningRequest("test")
	assert.NoError(t, err)
}

func TestCreateClusterRole(t *testing.T) {
	clientsets, err := NewTestK8sClientsets()
	assert.NoError(t, err)
	cr := &rbac.ClusterRole{
		ObjectMeta: metav1.ObjectMeta{Name: "nafigos"},
		Rules: []rbac.PolicyRule{
			{
				APIGroups: []string{""},
				Resources: []string{"configmaps", "endpoints", "namespaces", "nodes", "pods", "pods/log", "replicationcontrollers", "services"},
				Verbs:     []string{"get", "list", "watch"},
			},
		},
	}
	_, err = clientsets.CreateClusterRole(cr)
	assert.NoError(t, err)
}
