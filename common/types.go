package common

import (
	core "k8s.io/api/core/v1"
)

// NafigosBase is a base interface type for Nafigos entities that need to be stored
type NafigosBase interface {
	GetID() string
	GetCollection() string
	GetOwner() string
}

// BuildStatus is used to get information about a Build. Info will be a container's
// output log
type BuildStatus struct {
	Complete bool   `json:"complete"`
	Success  bool   `json:"success"`
	Info     string `json:"info"`
}

// BuildStep ...
type BuildStep struct {
	Image      string        `bson:"image" json:"image,omitempty" yaml:"image,omitempty"`
	Dockerfile string        `bson:"dockerfile" json:"dockerfile,omitempty" yaml:"dockerfile,omitempty"`
	Args       []core.EnvVar `bson:"args" json:"args,omitempty" yaml:"args,omitempty"`
}

// SecretType associates a type with a Secret
type SecretType string

const (
	// GCRSecret is used for Google Container Registry
	GCRSecret SecretType = "gcr"
	// DockerhubSecret is used for Dockerhub
	DockerhubSecret SecretType = "dockerhub"
	// GitSecret is used for Github, GitLab, etc.
	GitSecret SecretType = "git"
)

// Secret ...
type Secret struct {
	Username string     `json:"username,omitempty"`
	Value    string     `json:"value,omitempty"`
	Type     SecretType `json:"type,omitempty"`
	ID       string     `json:"id,omitempty"`
}

// GetRedacted will return a new Secret with the Value set to "REDACTED"
func (s *Secret) GetRedacted() Secret {
	redactedSecret := *s
	redactedSecret.Value = "REDACTED"
	return redactedSecret
}

// Cluster ...
type Cluster struct {
	ID               string `json:"id,omitempty"`
	Name             string `json:"name,omitempty"`
	DefaultNamespace string `json:"default_namespace,omitempty"`
	Config           string `json:"config,omitempty"`
	Host             string `json:"host,omitempty"`
	Slug             string `json:"slug,omitempty"`
	Admin            bool   `json:"admin,omitempty"`
}

// GetRedacted will return a new Cluster with the Value set to "REDACTED"
func (c *Cluster) GetRedacted() Cluster {
	redactedCluster := *c
	redactedCluster.Config = "REDACTED"
	return redactedCluster
}

// User defines user attributes, primarily Secrets that are written to Vault.
// Note that the Password field is only used for creating the User
type User struct {
	Username string             `json:"username,omitempty"`
	Password string             `json:"password,omitempty,omitempty"`
	Secrets  map[string]Secret  `json:"secrets,omitempty"`
	Clusters map[string]Cluster `json:"clusters,omitempty"`
	IsAdmin  bool               `json:"is_admin"`
}

// GetRedacted will return a new User with sensitive fields redacted
func (u *User) GetRedacted() User {
	redactedUser := *u
	redactedSecrets := map[string]Secret{}
	redactedClusters := map[string]Cluster{}

	for k := range redactedUser.Secrets {
		s := redactedUser.Secrets[k]
		s.Value = "REDACTED"
		redactedSecrets[k] = s
	}
	for k := range redactedUser.Clusters {
		c := redactedUser.Clusters[k]
		c.Config = "REDACTED"
		redactedClusters[k] = c
	}

	redactedUser.Secrets = redactedSecrets
	redactedUser.Clusters = redactedClusters
	redactedUser.Password = "REDACTED"
	return redactedUser
}

// Repository contains basic information required to interact with git
// The URL is the RawURL without the leading `https://` in order to use as an ID
// and the RawURL is saved for cloning
type Repository struct {
	RawURL string `bson:"rawurl" json:"rawurl,omitempty"`
	URL    string `bson:"url" json:"url,omitempty"`
	Branch string `bson:"branch" json:"branch,omitempty"`
}

// Error is used for responding to synchronous requests with an error that is
// JSON friendly
type Error struct {
	Message string `json:"message,omitempty"`
	Code    int    `json:"code,omitempty"`
}
