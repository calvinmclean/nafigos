package api

import (
	"fmt"
	"os"
	"regexp"

	"gitlab.com/cyverse/nafigos/api-service/api/keycloak"
	"gitlab.com/cyverse/nafigos/api-service/api/vault"
	"gitlab.com/cyverse/nafigos/common"
)

var (
	natsInfo       map[string]string
	vaultClient    vault.Adapter
	keycloakClient *keycloak.Client
	hmacSecret     []byte
)

// InitVaultClient is used to define Vault connection variables
func InitVaultClient(defaultAddress string) (err error) {
	address := os.Getenv("VAULT_ADDRESS")
	if len(address) == 0 {
		address = defaultAddress
	}
	vaultClient, err = vault.NewClient(
		address,
		os.Getenv("VAULT_ROOT_TOKEN"),
		os.Getenv("AUTH_METHOD"),
	)
	if err != nil {
		err = fmt.Errorf("error creating Vault client: %v", err)
		return
	}
	// Enable auth method
	if vaultClient.GetAuthMethod() != "keycloak" {
		err = vaultClient.EnableUserpassAuth()
	} else {
		err = vaultClient.EnableJWTAuth(keycloakClient.URL)
	}
	// Ignore error if it is just about path already existing
	if err != nil && !regexp.MustCompile(`path is already in use at`).MatchString(err.Error()) {
		err = fmt.Errorf("error enabling Vault auth method: %v", err)
		return
	}
	// Create admin policy
	_, err = vaultClient.CreateAdminPolicy()
	if err != nil {
		err = fmt.Errorf("error creating admin Vault policy: %v", err)
		return
	}
	return
}

// InitKeycloakClient is used to define Keycloak connection variables
func InitKeycloakClient(defaultURL, defaultRedirectURL, defaultClientID string) (err error) {
	address := os.Getenv("KEYCLOAK_URL")
	if len(address) == 0 {
		address = defaultURL
	}
	redirect := os.Getenv("KEYCLOAK_REDIRECT_URL")
	if len(redirect) == 0 {
		redirect = defaultRedirectURL
	}
	clientID := os.Getenv("KEYCLOAK_ID")
	if len(clientID) == 0 {
		clientID = defaultClientID
	}
	if os.Getenv("AUTH_METHOD") == "keycloak" {
		keycloakClient, err = keycloak.NewClient(
			address,
			redirect,
			clientID,
			os.Getenv("KEYCLOAK_SECRET"),
		)
	}
	hmacSecret = []byte(os.Getenv("HMAC_SECRET"))
	if len(hmacSecret) == 0 {
		hmacSecret = []byte("secret")
	}
	return
}

// InitNATSConnectionVars is used to define the NATS connection variables
func InitNATSConnectionVars(defaultClusterID, defaultClientID, defaultConnAddress string) {
	natsInfo = common.GetNATSInfo(defaultClusterID, defaultClientID, defaultConnAddress)
}
