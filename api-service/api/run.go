package api

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"time"

	"gitlab.com/cyverse/nafigos/common"
	"gitlab.com/cyverse/nafigos/phylax-service/phylax"
	pleo "gitlab.com/cyverse/nafigos/pleo/util"

	"github.com/gorilla/mux"
	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
)

var (
	maxRetry        = int64(10)
	waitTimeSeconds = int64(5)
)

// RunAPIRouter creates routes for interacting with the Pleo service for running
// WorkflowDefinitions in a Kubernetes cluster
func RunAPIRouter(router *mux.Router) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "RunAPIRouter",
	})

	// Check for environment variables overriding default values
	maxRetryString := os.Getenv("maxRetry")
	if len(maxRetryString) > 0 {
		val, err := strconv.ParseInt(maxRetryString, 10, 64)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Warnf("using default value of %d for maxRetry", maxRetry)
		} else {
			maxRetry = val
		}
	}
	waitTimeSecondsString := os.Getenv("waitTimeSeconds")
	if len(waitTimeSecondsString) > 0 {
		val, err := strconv.ParseInt(waitTimeSecondsString, 10, 64)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Warnf("using default value of %d for waitTimeSeconds", waitTimeSeconds)
		} else {
			waitTimeSeconds = val
		}
	}
	// Start handlers
	router.HandleFunc("/runs", getRunsForUser).Methods("GET")
	router.HandleFunc("/runs", startRun).Methods("POST")
	router.HandleFunc("/runs/{runID}", getRun).Methods("GET")
	router.HandleFunc("/runs/{runID}", updateRun).Methods("PUT") // TODO: What is this?
	router.HandleFunc("/runs/{runID}", deleteRun).Methods("DELETE")
	// This handler is used to scale from zero
	router.PathPrefix("/{runID}:{port}/").HandlerFunc(scaleFromZero)
	router.PathPrefix("/{runID}:{port}").HandlerFunc(scaleFromZero)
}

// scaleFromZero is a function that will be triggered when Istio routing on a user
// cluster is proxied here (meaning service is not running). This will then run
// the service in the user cluster and attempt to forward this request back to the
// running service once it is up
func scaleFromZero(w http.ResponseWriter, r *http.Request) {
	runID := mux.Vars(r)["runID"]
	logger := log.WithFields(log.Fields{
		"package":     "api",
		"function":    "scaleFromZero",
		"request_uri": r.RequestURI,
		"run_id":      runID,
	})
	logger.Info("received request to scale up workflow")

	// First create the request
	var request pleo.Request
	status, err := prepareRequest(&request, r, "")
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to parse and prepare request")
		jsonError(w, err.Error(), status)
		return
	}
	request.SetRunID(runID)
	logRequest := request
	logRequest.BasicRequest = logRequest.GetRedacted()
	logger = logger.WithFields(log.Fields{"request": logRequest})
	logger.Info("parsed request")

	// Get Runner info from Phylax
	msg, err := common.PublishRequest(
		&request,
		"Phylax.GetRunner",
		"API",
		natsInfo["address"],
	)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to publish request to scale up workflow")
		jsonError(w, "unable to publish request to scale up workflow: "+err.Error(), http.StatusInternalServerError)
		return
	}

	var runnerResponse struct {
		Error  common.Error   `json:"error,omitempty"`
		Runner *phylax.Runner `json:"runner"`
	}
	err = json.Unmarshal(msg, &runnerResponse)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to read runner response")
		jsonError(w, "unable to read runner response: "+err.Error(), http.StatusInternalServerError)
		return
	}
	if len(runnerResponse.Error.Message) > 0 {
		err = fmt.Errorf(runnerResponse.Error.Message)
		logger.WithFields(log.Fields{"error": err}).Error("received error response when getting runner")
		jsonError(w, "received error response when getting runner: "+err.Error(), http.StatusInternalServerError)
		return
	}

	// Now get WorkflowDefinition information using WorkflowID
	request.SetWorkflowID(runnerResponse.Runner.WorkflowDefinitionID)
	logRunner := runnerResponse.Runner.GetRedacted()
	logRequest = request
	logRequest.BasicRequest = logRequest.GetRedacted()
	logger = logger.WithFields(log.Fields{
		"request": logRequest,
		"runner":  logRunner,
	})
	logger.Info("successfully got Runner")

	status, err = getWorkflowDefinitionData(&request)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to get WorkflowDefinition data")
		jsonError(w, "unable to get WorkflowDefinition data: "+err.Error(), status)
		return
	}

	// Set other request fields from runnerResponse
	request.SetClusterID(runnerResponse.Runner.Cluster.ID)
	user := request.GetUser()
	user.Clusters = map[string]common.Cluster{
		request.GetClusterID(): runnerResponse.Runner.Cluster,
	}
	request.SetUser(user)
	request.Namespace = runnerResponse.Runner.Namespace
	request.TCPPorts = runnerResponse.Runner.TCPPorts
	request.HTTPPorts = runnerResponse.Runner.HTTPPorts
	request.AuthDisabled = runnerResponse.Runner.AuthDisabled

	logRequest = request
	logRequest.BasicRequest = logRequest.GetRedacted()
	logger = logger.WithFields(log.Fields{"request": logRequest})

	msg, err = json.Marshal(request)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to marshal request")
		jsonError(w, "unable to marshal request: "+err.Error(), http.StatusInternalServerError)
		return
	}
	err = common.StreamingPublish(msg, natsInfo, "Pleo.ScaleUp", "API")
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to publish scale up request")
		jsonError(w, "unable to publish scale up request: "+err.Error(), http.StatusInternalServerError)
		return
	}

	// Now retry the request after resetting the Request URI
	reqURL, err := url.Parse("http://" + r.Host + r.URL.String())
	if err != nil {
		logger.Warn("unable to parse URL:", err)
	}
	r.RequestURI = ""
	r.URL = reqURL

	i := int64(1)
	for ; i <= maxRetry; i++ {
		time.Sleep(time.Duration(waitTimeSeconds) * time.Second)
		logger.Info("retrying request used to trigger scaling")
		resp, err := (&http.Client{}).Do(r)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to execute retry request")
			logger.Infof("attempt %d/%d failed\n", i, maxRetry)
		} else {
			logger.Infof("attempt %d/%d succeeded\n", i, maxRetry)
			// Now read response body into bytes and write back to the user
			if resp.Body != nil {
				var respBody []byte
				respBody, err = ioutil.ReadAll(resp.Body)
				if err != nil {
					jsonError(w, err.Error(), http.StatusInternalServerError)
					return
				}
				w.Write(respBody)
			}
			return
		}
	}
	// If i exceeds maxRetry, then this failed
	if i > maxRetry && err != nil {
		jsonError(w, err.Error(), http.StatusBadRequest)
	}
}

// startRun is used to run a WorkflowDefinition on the user's cluster
func startRun(w http.ResponseWriter, r *http.Request) {
	runID := xid.New().String()
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "startRun",
		"run_id":   runID,
	})
	logger.Info("received request to start run")

	var request pleo.Request
	status, err := prepareRequest(&request, r, "")
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to parse and prepare request")
		jsonError(w, err.Error(), status)
		return
	}
	logRequest := request
	logRequest.BasicRequest = logRequest.GetRedacted()
	logger = logger.WithFields(log.Fields{"request": logRequest})
	logger.Info("parsed request")

	// Check that necessary details were included in the request
	if len(request.GetWorkflowID()) == 0 {
		err = fmt.Errorf("required workflow ID was not provided")
		logger.WithFields(log.Fields{"error": err}).Error("unable to run Workflow without workflow ID")
		jsonError(w, err.Error(), http.StatusBadRequest)
		return
	}
	if len(request.GetClusterID()) == 0 {
		err = fmt.Errorf("required cluster ID was not provided")
		logger.WithFields(log.Fields{"error": err}).Error("unable to run Workflow without cluster ID")
		jsonError(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Now get WorkflowDefinition data
	status, err = getWorkflowDefinitionData(&request)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to get WorkflowDefinition data")
		jsonError(w, "unable to get WorkflowDefinition data: "+err.Error(), status)
		return
	}

	// Make sure WorkflowDefinition error is not in an Error state before continuing
	if len(request.WorkflowDefinition.Error) > 0 {
		log.Println("Unable to run WorkflowDefinition in error state")
		jsonError(w, "Unable to run WorkflowDefinition in an error state. Please fix Workflow and try again", http.StatusBadRequest)
		return
	}

	// Generate XID for this execution
	request.SetRunID(runID)

	logRequest = request
	logRequest.BasicRequest = logRequest.GetRedacted()
	logger = logger.WithFields(log.Fields{"request": logRequest})
	logger.Info("generated run ID")

	msg, err := json.Marshal(request)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to marshal request JSON")
		jsonError(w, "unable to marshal request JSON: "+err.Error(), http.StatusInternalServerError)
		return
	}

	err = common.StreamingPublish(msg, natsInfo, "Pleo.Run", "API")
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to publish Run request")
		jsonError(w, "unable to publish Run request: "+err.Error(), http.StatusInternalServerError)
		return
	}
	idResponse(w, request.GetRunID(), http.StatusAccepted)
}

// deleteRun will delete the running service in the user cluster along with any
// additional resources created alongside it
func deleteRun(w http.ResponseWriter, r *http.Request) {
	runID := mux.Vars(r)["runID"]
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "deleteRun",
		"run_id":   runID,
	})
	logger.Info("received request to delete run")

	var request pleo.Request
	status, err := prepareRequest(&request, r, "")
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to parse and prepare request")
		jsonError(w, err.Error(), status)
		return
	}
	request.SetRunID(runID)

	logRequest := request
	logRequest.BasicRequest = logRequest.GetRedacted()
	logger = logger.WithFields(log.Fields{"request": logRequest})
	logger.Info("parsed request")

	msg, err := json.Marshal(request)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to marshal request JSON")
		jsonError(w, "unable to marshal request JSON: "+err.Error(), http.StatusInternalServerError)
		return
	}

	err = common.StreamingPublish(msg, natsInfo, "Pleo.Delete", "API")
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to publish delete request")
		jsonError(w, "unable to publish delete request: "+err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}

func getRunsForUser(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getRunsForUser",
	})
	logger.Info("received request to get runs for a user")

	var request phylax.Request
	status, err := prepareRequest(&request, r, "")
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to parse and prepare request")
		jsonError(w, err.Error(), status)
		return
	}
	logRequest := request
	logRequest.BasicRequest = logRequest.GetRedacted()
	logger = logger.WithFields(log.Fields{"request": logRequest})
	logger.Info("parsed request")

	// Get info from Phylax
	msg, err := common.PublishRequest(
		&request,
		"Phylax.GetRunnersForUser",
		"API",
		natsInfo["address"],
	)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to publish request to get runs")
		jsonError(w, "unable to publish request to get runs: "+err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(getResponseCode(msg))
	w.Write(msg)
}

func getRun(w http.ResponseWriter, r *http.Request) {
	runID := mux.Vars(r)["runID"]
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getRun",
		"run_id":   runID,
	})
	logger.Info("received request to get a run")

	var request phylax.Request
	status, err := prepareRequest(&request, r, "")
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to parse and prepare request")
		jsonError(w, err.Error(), status)
		return
	}
	request.SetRunID(runID)

	logRequest := request
	logRequest.BasicRequest = logRequest.GetRedacted()
	logger = logger.WithFields(log.Fields{"request": logRequest})
	logger.Info("parsed request")

	// Get info from Phylax
	msg, err := common.PublishRequest(
		&request,
		"Phylax.GetRunner",
		"API",
		natsInfo["address"],
	)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to publish request to get run")
		jsonError(w, "unable to publish request to get run: "+err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(getResponseCode(msg))
	w.Write(msg)
}

func updateRun(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "updateRun",
	})
	logger.Info("received request to update a run")

	jsonError(w, "feature not implemented", http.StatusNotImplemented)
}
