package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"regexp"
	"sync"
	"testing"
	"time"

	"gitlab.com/cyverse/nafigos/common"

	"github.com/gorilla/mux"
	"github.com/nats-io/nats.go"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func testGetWorkflowDefinitionsForUser(t *testing.T) {
	s := runNATSServer(t)
	defer s.Shutdown()

	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)

	var wg sync.WaitGroup
	wg.Add(1)
	go common.SynchronousTestSubscriber(
		"WorkflowDefinition.GetForUser",
		"WorkflowDefinition.GetForUserQueue",
		func(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
			var request struct {
				User common.User `json:"user"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, "test_username", request.User.Username)
			assert.Empty(t, request.User.Secrets)
			msg.Respond([]byte(`{"workflow_definitions": {}}`))
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "workflow-definition",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	time.Sleep(100 * time.Millisecond)

	// Create request
	req, err := http.NewRequest("GET", "/workflows", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{
	"url": "https://github.com/username/project",
	"branch": "branch"
}`)))

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/workflows", getWorkflowDefinitionsForUser)
	router.ServeHTTP(rr, req)
	wg.Wait()

	assert.Equal(t, http.StatusOK, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
}

func testGetWorkflowDefinitionsForUserBadRequest(t *testing.T) {
	// Create request
	req, err := http.NewRequest("GET", "/workflows", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{`)))

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/workflows", getWorkflowDefinitionsForUser)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 400, "message": "Error reading request body: unexpected end of JSON input"}}`, rr.Body.String())
}

func testGetWorkflowDefinitionsForUserError(t *testing.T) {
	s := runNATSServer(t)
	defer s.Shutdown()

	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)

	var wg sync.WaitGroup
	wg.Add(1)
	go common.SynchronousTestSubscriber(
		"WorkflowDefinition.GetForUser",
		"WorkflowDefinition.GetForUserQueue",
		func(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
			var request struct {
				User common.User `json:"user"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, "test_username", request.User.Username)
			assert.Empty(t, request.User.Secrets)
			msg.Respond([]byte(`{"error": {"code": 500, "message": "mock error"}}`))
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "workflow-definition",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	time.Sleep(100 * time.Millisecond)

	// Create request
	req, err := http.NewRequest("GET", "/workflows", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{
	"url": "https://github.com/username/project",
	"branch": "branch"
}`)))

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/workflows", getWorkflowDefinitionsForUser)
	router.ServeHTTP(rr, req)
	wg.Wait()

	assert.Equal(t, http.StatusInternalServerError, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 500, "message": "mock error"}}`, rr.Body.String())
}

func testGetWorkflowDefinitionsForUserNATSError(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)

	req, err := http.NewRequest("GET", "/workflows", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/workflows", getWorkflowDefinitionsForUser)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusInternalServerError, rr.Code)
	assert.Equal(t, `{"error": {"code": 500, "message": "error connecting to NATS: nats: no servers available for connection"}}`, rr.Body.String())
}

func testCreateWorkflowDefinition(t *testing.T) {
	s := runNATSServer(t)
	defer s.Shutdown()

	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)

	var wg sync.WaitGroup
	wg.Add(1)
	go common.StreamingTestSubscriber(
		"WorkflowDefinition.Create",
		"WorkflowDefinition.CreateQueue",
		func(reqBytes []byte, natsInfo map[string]string) {
			var request struct {
				User   common.User `json:"user"`
				URL    string      `json:"url"`
				Branch string      `json:"branch"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, "branch", request.Branch)
			assert.Equal(t, "https://github.com/username/project", request.URL)
			assert.Equal(t, "test_username", request.User.Username)
			assert.Empty(t, request.User.Secrets)
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "workflow-definition",
			"address":    "nats://localhost:4222",
		}, &wg,
	)

	// Create request
	req, err := http.NewRequest("POST", "/workflows", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{
	"url": "https://github.com/username/project",
	"branch": "branch"
}`)))

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/workflows", createWorkflowDefinition)
	router.ServeHTTP(rr, req)
	wg.Wait()

	assert.Equal(t, http.StatusAccepted, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Regexp(t, regexp.MustCompile(`{"id": "[a-z0-9]{20}"}`), rr.Body.String())
}

func testCreateWorkflowDefinitionNoData(t *testing.T) {
	s := runNATSServer(t)
	defer s.Shutdown()

	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)

	// Create request
	req, err := http.NewRequest("POST", "/workflows", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/workflows", createWorkflowDefinition)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 400, "message": "'url' is missing from request but is required"}}`, rr.Body.String())
}

func testCreateWorkflowDefinitionBadData(t *testing.T) {
	s := runNATSServer(t)
	defer s.Shutdown()

	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)

	// Create request
	req, err := http.NewRequest("POST", "/workflows", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{`)))

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/workflows", createWorkflowDefinition)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 400, "message": "Error reading request body: unexpected end of JSON input"}}`, rr.Body.String())
}

func testCreateWorkflowDefinitionWithSecret(t *testing.T) {
	s := runNATSServer(t)
	defer s.Shutdown()

	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("ReadSecret", "test_username", "9m4e2mr0ui3e8a215n4g", "vault_token").
		Return(common.Secret{
			ID:       "9m4e2mr0ui3e8a215n4g",
			Type:     common.GitSecret,
			Username: "test_githubusername",
			Value:    "test_githubtoken",
		}, 200, nil)

	var wg sync.WaitGroup
	wg.Add(1)
	go common.StreamingTestSubscriber(
		"WorkflowDefinition.Create",
		"WorkflowDefinition.CreateQueue",
		func(reqBytes []byte, natsInfo map[string]string) {
			var request struct {
				User   common.User `json:"user"`
				Branch string      `json:"branch"`
				URL    string      `json:"url"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, "branch", request.Branch)
			assert.Equal(t, "https://github.com/username/project", request.URL)
			assert.Equal(t, "test_username", request.User.Username)
			assert.NotEmpty(t, request.User.Secrets)
			assert.Equal(t, common.Secret{
				ID:       "9m4e2mr0ui3e8a215n4g",
				Type:     common.GitSecret,
				Username: "test_githubusername",
				Value:    "test_githubtoken",
			}, request.User.Secrets["9m4e2mr0ui3e8a215n4g"])
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "workflow-definition",
			"address":    "nats://localhost:4222",
		}, &wg,
	)

	// Create request
	req, err := http.NewRequest("POST", "/workflows", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{
	"url": "https://github.com/username/project",
	"branch": "branch",
	"git_secret_id": "9m4e2mr0ui3e8a215n4g"
}`)))

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/workflows", createWorkflowDefinition)
	router.ServeHTTP(rr, req)
	wg.Wait()

	assert.Equal(t, http.StatusAccepted, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Regexp(t, regexp.MustCompile(`{"id": "[a-z0-9]{20}"}`), rr.Body.String())
}

func testCreateWorkflowDefinitionWithSecretError(t *testing.T) {
	s := runNATSServer(t)
	defer s.Shutdown()

	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("ReadSecret", "test_username", "9m4e2mr0ui3e8a215n4g", "vault_token").
		Return(common.Secret{}, 403, fmt.Errorf("permission denied"))

	// Create request
	req, err := http.NewRequest("POST", "/workflows", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{
	"url": "https://github.com/username/project",
	"branch": "branch",
	"git_secret_id": "9m4e2mr0ui3e8a215n4g"
}`)))

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/workflows", createWorkflowDefinition)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusForbidden, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 403, "message": "Error reading git secret '9m4e2mr0ui3e8a215n4g': permission denied"}}`, rr.Body.String())
}

func testGetWorkflowDefinition(t *testing.T) {
	s := runNATSServer(t)
	defer s.Shutdown()

	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)

	var wg sync.WaitGroup
	wg.Add(1)
	go common.SynchronousTestSubscriber(
		"WorkflowDefinition.Get",
		"WorkflowDefinition.GetQueue",
		func(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
			var request struct {
				User       common.User `json:"user"`
				WorkflowID string      `json:"workflow_id"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, "9m4e2mr0ui3e8a215n4g", request.WorkflowID)
			assert.Equal(t, "test_username", request.User.Username)
			assert.Empty(t, request.User.Secrets)
			msg.Respond([]byte(`{"workflow_definition": {}}`))
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "workflow-definition",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	time.Sleep(100 * time.Millisecond)

	// Create request
	req, err := http.NewRequest("GET", "/workflows/9m4e2mr0ui3e8a215n4g", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/workflows/{workflowID}", getWorkflowDefinition)
	router.ServeHTTP(rr, req)
	wg.Wait()

	assert.Equal(t, http.StatusOK, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
}

func testGetWorkflowDefinitionBadRequest(t *testing.T) {
	req, err := http.NewRequest("GET", "/workflows/9m4e2mr0ui3e8a215n4g", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{`)))

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/workflows/{workflowID}", getWorkflowDefinition)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 400, "message": "Error reading request body: unexpected end of JSON input"}}`, rr.Body.String())
}

func testGetWorkflowDefinitionNATSError(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)

	req, err := http.NewRequest("GET", "/workflows/9m4e2mr0ui3e8a215n4g", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/workflows/{workflowID}", getWorkflowDefinition)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusInternalServerError, rr.Code)
	assert.Equal(t, `{"error": {"code": 500, "message": "error connecting to NATS: nats: no servers available for connection"}}`, rr.Body.String())
}

func testGetWorkflowDefinitionError(t *testing.T) {
	s := runNATSServer(t)
	defer s.Shutdown()

	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)

	var wg sync.WaitGroup
	wg.Add(1)
	go common.SynchronousTestSubscriber(
		"WorkflowDefinition.Get",
		"WorkflowDefinition.GetQueue",
		func(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
			var request struct {
				User       common.User `json:"user"`
				WorkflowID string      `json:"workflow_id"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, "9m4e2mr0ui3e8a215n4g", request.WorkflowID)
			assert.Equal(t, "test_username", request.User.Username)
			assert.Empty(t, request.User.Secrets)
			msg.Respond([]byte(`{"error": {"code": 500, "message": "mock error"}}`))
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "workflow-definition",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	time.Sleep(100 * time.Millisecond)

	// Create request
	req, err := http.NewRequest("GET", "/workflows/9m4e2mr0ui3e8a215n4g", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/workflows/{workflowID}", getWorkflowDefinition)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusInternalServerError, rr.Code)
	assert.Equal(t, `{"error": {"code": 500, "message": "mock error"}}`, rr.Body.String())
}

func testDeleteWorkflowDefinition(t *testing.T) {
	s := runNATSServer(t)
	defer s.Shutdown()

	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)

	var wg sync.WaitGroup
	wg.Add(2)
	go common.SynchronousTestSubscriber(
		"WorkflowDefinition.Get",
		"WorkflowDefinition.GetQueue",
		func(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
			var request struct {
				User       common.User `json:"user"`
				WorkflowID string      `json:"workflow_id"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, "9m4e2mr0ui3e8a215n4g", request.WorkflowID)
			assert.Equal(t, "test_username", request.User.Username)
			assert.Empty(t, request.User.Secrets)
			msg.Respond([]byte(`{"workflow_definition": {}}`))
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "workflow-definition",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	go common.StreamingTestSubscriber(
		"WorkflowDefinition.Delete",
		"WorkflowDefinition.DeleteQueue",
		func(reqBytes []byte, natsInfo map[string]string) {
			var request struct {
				User       common.User `json:"user"`
				WorkflowID string      `json:"workflow_id"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, "9m4e2mr0ui3e8a215n4g", request.WorkflowID)
			assert.Equal(t, "test_username", request.User.Username)
			assert.Empty(t, request.User.Secrets)
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "workflow-definition",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	time.Sleep(100 * time.Millisecond)

	// Create request
	req, err := http.NewRequest("DELETE", "/workflows/9m4e2mr0ui3e8a215n4g", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/workflows/{workflowID}", deleteWorkflowDefinition)
	router.ServeHTTP(rr, req)
	wg.Wait()

	assert.Equal(t, http.StatusNoContent, rr.Code)
	assert.Empty(t, rr.Body.String())
}

func testDeleteWorkflowDefinitionError(t *testing.T) {
	s := runNATSServer(t)
	defer s.Shutdown()

	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)

	var wg sync.WaitGroup
	wg.Add(1)
	go common.SynchronousTestSubscriber(
		"WorkflowDefinition.Get",
		"WorkflowDefinition.GetQueue",
		func(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
			msg.Respond([]byte(`{"error": {"code": 500, "message": "mock error"}}`))
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "workflow-definition",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	time.Sleep(100 * time.Millisecond)

	// Create request
	req, err := http.NewRequest("DELETE", "/workflows/9m4e2mr0ui3e8a215n4g", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{"name": "new-name"}`)))

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/workflows/{workflowID}", deleteWorkflowDefinition)
	router.ServeHTTP(rr, req)
	wg.Wait()

	assert.Equal(t, http.StatusInternalServerError, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 500, "message": "mock error"}}`, rr.Body.String())
}

func testUpdateWorkflowDefinition(t *testing.T) {
	s := runNATSServer(t)
	defer s.Shutdown()

	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("ReadSecret", "test_username", "9m4e2mr0ui3e8a215n4g", "vault_token").
		Return(common.Secret{
			ID:       "9m4e2mr0ui3e8a215n4g",
			Type:     common.GitSecret,
			Username: "test_githubusername",
			Value:    "test_githubtoken",
		}, 200, nil)

	var wg sync.WaitGroup
	wg.Add(2)
	go common.SynchronousTestSubscriber(
		"WorkflowDefinition.Get",
		"WorkflowDefinition.GetQueue",
		func(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
			msg.Respond([]byte(`{"workflow_definition": {}}`))
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "workflow-definition",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	go common.StreamingTestSubscriber(
		"WorkflowDefinition.Update",
		"WorkflowDefinition.UpdateQueue",
		func(reqBytes []byte, natsInfo map[string]string) {
			var request struct {
				User common.User `json:"user"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, "test_username", request.User.Username)
			assert.NotEmpty(t, request.User.Secrets)
			assert.Equal(t, common.Secret{
				ID:       "9m4e2mr0ui3e8a215n4g",
				Type:     common.GitSecret,
				Username: "test_githubusername",
				Value:    "test_githubtoken",
			}, request.User.Secrets["9m4e2mr0ui3e8a215n4g"])
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "workflow-definition",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	time.Sleep(100 * time.Millisecond)

	// Create request
	req, err := http.NewRequest("PUT", "/workflows/9m4e2mr0ui3e8a215n4g", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{"git_secret_id": "9m4e2mr0ui3e8a215n4g"}`)))

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/workflows/{workflowID}", updateWorkflowDefinition)
	router.ServeHTTP(rr, req)
	wg.Wait()

	assert.Equal(t, http.StatusAccepted, rr.Code)
	assert.Empty(t, rr.Body.String())
}

func testUpdateWorkflowDefinitionNoSecret(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)

	// Create request
	req, err := http.NewRequest("PUT", "/workflows/9m4e2mr0ui3e8a215n4g", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{"name": "new-name"}`)))
	req.ContentLength = int64(len(`{"name": "new-name"}`))

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/workflows/{workflowID}", updateWorkflowDefinition)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
	assert.Equal(t, `{"error": {"code": 400, "message": "git secret ID is required to update a WorkflowDefinition"}}`, rr.Body.String())
}

func testUpdateWorkflowDefinitionError(t *testing.T) {
	s := runNATSServer(t)
	defer s.Shutdown()

	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("ReadSecret", "test_username", "9m4e2mr0ui3e8a215n4g", "vault_token").
		Return(common.Secret{
			ID:       "9m4e2mr0ui3e8a215n4g",
			Type:     common.GitSecret,
			Username: "test_githubusername",
			Value:    "test_githubtoken",
		}, 200, nil)

	var wg sync.WaitGroup
	wg.Add(1)
	go common.SynchronousTestSubscriber(
		"WorkflowDefinition.Get",
		"WorkflowDefinition.GetQueue",
		func(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
			msg.Respond([]byte(`{"error": {"code": 500, "message": "mock error"}}`))
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "workflow-definition",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	time.Sleep(100 * time.Millisecond)

	// Create request
	req, err := http.NewRequest("PUT", "/workflows/9m4e2mr0ui3e8a215n4g", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{"name": "new-name", "git_secret_id": "9m4e2mr0ui3e8a215n4g"}`)))

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/workflows/{workflowID}", updateWorkflowDefinition)
	router.ServeHTTP(rr, req)
	wg.Wait()

	assert.Equal(t, http.StatusInternalServerError, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 500, "message": "unable to get WorkflowDefinition data: mock error"}}`, rr.Body.String())
}

func testUpdateWorkflowDefinitionNoData(t *testing.T) {
	s := runNATSServer(t)
	defer s.Shutdown()

	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)

	var wg sync.WaitGroup
	wg.Add(2)
	go common.SynchronousTestSubscriber(
		"WorkflowDefinition.Get",
		"WorkflowDefinition.GetQueue",
		func(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
			msg.Respond([]byte(`{"workflow_definition": {}}`))
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "workflow-definition",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	go common.StreamingTestSubscriber(
		"WorkflowDefinition.Update",
		"WorkflowDefinition.UpdateQueue",
		func(reqBytes []byte, natsInfo map[string]string) {
			var request struct {
				User common.User `json:"user"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, "test_username", request.User.Username)
			assert.Empty(t, request.User.Secrets)
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "workflow-definition",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	time.Sleep(100 * time.Millisecond)

	// Create request
	req, err := http.NewRequest("PUT", "/workflows/9m4e2mr0ui3e8a215n4g", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/workflows/{workflowID}", updateWorkflowDefinition)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusAccepted, rr.Code)
	assert.Empty(t, rr.Body.String())
}

func testUpdateWorkflowDefinitionBadData(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)

	// Create request
	req, err := http.NewRequest("PUT", "/workflows/9m4e2mr0ui3e8a215n4g", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{`)))

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/workflows/{workflowID}", updateWorkflowDefinition)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 400, "message": "Error reading request body: unexpected end of JSON input"}}`, rr.Body.String())
}
