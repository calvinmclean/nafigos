package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"sync"
	"testing"
	"time"

	"gitlab.com/cyverse/nafigos/common"

	"github.com/gorilla/mux"
	"github.com/nats-io/nats.go"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func testGetAllKclusters(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("GetAllClusters", "test_username", "vault_token").
		Return(map[string]common.Cluster{
			"9m4e2mr0ui3e8a215n4g": {
				ID:               "9m4e2mr0ui3e8a215n4g",
				Name:             "default",
				DefaultNamespace: "default",
				Config:           "config",
			},
		}, 200, nil)

	// Create request
	req, err := http.NewRequest("GET", "/kclusters", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/kclusters", getAllClusters)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code)
	assert.Equal(t, `{"9m4e2mr0ui3e8a215n4g":{"id":"9m4e2mr0ui3e8a215n4g","name":"default","default_namespace":"default","config":"config"}}`, rr.Body.String())
}

func testGetAllKclustersAuthError(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "", "test_password").
		Return("", nil)

	// Create request
	req, err := http.NewRequest("GET", "/kclusters", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = ""

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/kclusters", getAllClusters)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusUnauthorized, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 401, "message": "unable to get info for authenticated user"}}`, rr.Body.String())
}

func testGetAllKclustersPermissionDenied(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "bad_username", "test_password2").
		Return("vault_token", nil)
	vaultClient.GetMock().On("GetAllClusters", "bad_username", "vault_token").
		Return(map[string]common.Cluster{}, 403, fmt.Errorf("permission denied"))

	// Create request
	req, err := http.NewRequest("GET", "/kclusters", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password2")
	tokenStore["test_password2"] = "bad_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/kclusters", getAllClusters)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusForbidden, rr.Code)
	assert.Equal(t, `{"error": {"code": 403, "message": "unable to get kclusters: permission denied"}}`, rr.Body.String())
}

func testAddNewKcluster(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("AddCluster", "test_username", mock.Anything, common.Cluster{
		Name:             "default",
		DefaultNamespace: "default",
		Config:           "config",
		Slug:             "default",
	}, "vault_token").Return(200, nil)

	// Create request
	req, err := http.NewRequest("POST", "/kclusters", nil)
	assert.NoError(t, err)
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{
	"name": "default",
	"default_namespace": "default",
	"config": "config"
}`)))
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/kclusters", addNewCluster)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusCreated, rr.Code)
	assert.Regexp(t, `{"id":"[a-z0-9]{20}","name":"default","default_namespace":"default","config":"config","slug":"default"}`, rr.Body.String())
}

func testAddNewKclusterAuthError(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "", "test_password").
		Return("", nil)

	// Create request
	req, err := http.NewRequest("POST", "/kclusters", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = ""

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/kclusters", addNewCluster)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusUnauthorized, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 401, "message": "unable to get info for authenticated user"}}`, rr.Body.String())
}

func testAddNewKclusterBadRequest(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)

	// Create request
	req, err := http.NewRequest("POST", "/kclusters", nil)
	assert.NoError(t, err)
	// Bad JSON request body
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{`)))
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/kclusters", addNewCluster)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
	assert.Equal(t, `{"error": {"code": 400, "message": "unable to parse request into kcluster: unexpected end of JSON input"}}`, rr.Body.String())
}

func testAddNewKclusterPermissionDenied(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "bad_username", "test_password2").
		Return("vault_token", nil)
	vaultClient.GetMock().On("AddCluster", "bad_username", mock.Anything, common.Cluster{
		Name:             "default",
		DefaultNamespace: "default",
		Config:           "config",
		Slug:             "default",
	}, "vault_token").Return(403, fmt.Errorf("permission denied"))

	// Create request
	req, err := http.NewRequest("POST", "/kclusters", nil)
	assert.NoError(t, err)
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{
	"name": "default",
	"default_namespace": "default",
	"config": "config"
}`)))
	req.Header.Add("Authorization", "test_password2")
	tokenStore["test_password2"] = "bad_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/kclusters", addNewCluster)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusForbidden, rr.Code)
	assert.Equal(t, `{"error": {"code": 403, "message": "unable to write kcluster: permission denied"}}`, rr.Body.String())
}

func testGetKcluster(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("ReadCluster", "test_username", "9m4e2mr0ui3e8a215n4g", "vault_token").
		Return(common.Cluster{
			ID:               "9m4e2mr0ui3e8a215n4g",
			Name:             "default",
			DefaultNamespace: "default",
			Config:           "config",
			Slug:             "default",
		}, 200, nil)

	// Create request
	req, err := http.NewRequest("GET", "/kclusters/9m4e2mr0ui3e8a215n4g", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/kclusters/{clusterID}", getCluster)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code)
	assert.Equal(t, `{"id":"9m4e2mr0ui3e8a215n4g","name":"default","default_namespace":"default","config":"config","slug":"default"}`, rr.Body.String())
}

func testGetKclusterAuthError(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "", "test_password").
		Return("", nil)

	// Create request
	req, err := http.NewRequest("GET", "/kclusters/9m4e2mr0ui3e8a215n4g", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = ""

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/kclusters/{clusterID}", getCluster)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusUnauthorized, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 401, "message": "unable to get info for authenticated user"}}`, rr.Body.String())
}

func testGetKclusterPermissionDenied(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "bad_username", "test_password2").
		Return("vault_token", nil)
	vaultClient.GetMock().On("ReadCluster", "bad_username", "9m4e2mr0ui3e8a215n4g", "vault_token").
		Return(common.Cluster{}, 403, fmt.Errorf("permission denied"))

	// Create request
	req, err := http.NewRequest("GET", "/kclusters/9m4e2mr0ui3e8a215n4g", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password2")
	tokenStore["test_password2"] = "bad_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/kclusters/{clusterID}", getCluster)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusForbidden, rr.Code)
	assert.Equal(t, `{"error": {"code": 403, "message": "unable to get kcluster: permission denied"}}`, rr.Body.String())
}

func testUpdateKcluster(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On(
		"EditCluster",
		"test_username",
		"9m4e2mr0ui3e8a215n4g",
		common.Cluster{Config: "new_config"},
		"vault_token",
	).Return(200, nil)

	// Create request
	req, err := http.NewRequest("PUT", "/kclusters/9m4e2mr0ui3e8a215n4g", nil)
	assert.NoError(t, err)
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{"config": "new_config"}`)))
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/kclusters/{clusterID}", updateCluster)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusCreated, rr.Code)
	assert.Empty(t, rr.Body.String())
}

func testUpdateKclusterAuthError(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "", "test_password").
		Return("", nil)

	// Create request
	req, err := http.NewRequest("PUT", "/kclusters/9m4e2mr0ui3e8a215n4g", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = ""

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/kclusters/{clusterID}", updateCluster)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusUnauthorized, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 401, "message": "unable to get info for authenticated user"}}`, rr.Body.String())
}

func testUpdateKclusterBadRequest(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)

	// Create request
	req, err := http.NewRequest("PUT", "/kclusters/9m4e2mr0ui3e8a215n4g", nil)
	assert.NoError(t, err)
	// Bad JSON request body
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{`)))
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/kclusters/{clusterID}", updateCluster)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusBadRequest, rr.Code)
	assert.Equal(t, `{"error": {"code": 400, "message": "unable to parse request into kcluster: unexpected end of JSON input"}}`, rr.Body.String())
}

func testUpdateKclusterPermissionDenied(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "bad_username", "test_password2").
		Return("vault_token", nil)
	vaultClient.GetMock().On(
		"EditCluster",
		"bad_username",
		"9m4e2mr0ui3e8a215n4g",
		common.Cluster{Config: "new_config"},
		"vault_token",
	).Return(403, fmt.Errorf("permission denied"))

	// Create request
	req, err := http.NewRequest("PUT", "/kclusters/9m4e2mr0ui3e8a215n4g", nil)
	assert.NoError(t, err)
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(`{"config": "new_config"}`)))
	req.Header.Add("Authorization", "test_password2")
	tokenStore["test_password2"] = "bad_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/kclusters/{clusterID}", updateCluster)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusForbidden, rr.Code)
	assert.Equal(t, `{"error": {"code": 403, "message": "unable to update kcluster '9m4e2mr0ui3e8a215n4g': permission denied"}}`, rr.Body.String())
}

func testDeleteKcluster(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("DeleteCluster", "test_username", "9m4e2mr0ui3e8a215n4g", "vault_token").
		Return(200, nil)

	// Create request
	req, err := http.NewRequest("DELETE", "/kclusters/9m4e2mr0ui3e8a215n4g", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/kclusters/{clusterID}", deleteCluster)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusNoContent, rr.Code)
	assert.Empty(t, rr.Body.String())
}

func testDeleteKclusterAuthError(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "", "test_password").
		Return("", nil)

	// Create request
	req, err := http.NewRequest("DELETE", "/kclusters/9m4e2mr0ui3e8a215n4g", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = ""

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/kclusters/{clusterID}", deleteCluster)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusUnauthorized, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 401, "message": "unable to get info for authenticated user"}}`, rr.Body.String())
}

func testDeleteKclusterPermissionDenied(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "bad_username", "test_password2").
		Return("vault_token", nil)
	vaultClient.GetMock().On("DeleteCluster", "bad_username", "9m4e2mr0ui3e8a215n4g", "vault_token").
		Return(403, fmt.Errorf("permission denied"))

	// Create request
	req, err := http.NewRequest("DELETE", "/kclusters/9m4e2mr0ui3e8a215n4g", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password2")
	tokenStore["test_password2"] = "bad_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/kclusters/{clusterID}", deleteCluster)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusForbidden, rr.Code)
	assert.Equal(t, `{"error": {"code": 403, "message": "unable to delete cluster '9m4e2mr0ui3e8a215n4g': permission denied"}}`, rr.Body.String())
}

func testGetClusterResources(t *testing.T) {
	s := runNATSServer(t)
	defer s.Shutdown()

	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("ReadCluster", "test_username", "9m4e2mr0ui3e8a215n4g", "vault_token").
		Return(common.Cluster{
			ID:               "9m4e2mr0ui3e8a215n4g",
			Name:             "default",
			DefaultNamespace: "default",
			Config:           "config",
			Slug:             "default",
		}, 200, nil)

	var wg sync.WaitGroup
	wg.Add(1)
	go common.SynchronousTestSubscriber(
		"Phylax.GetResources",
		"Phylax.GetResourcesQueue",
		func(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
			var request struct {
				User      common.User `json:"user"`
				ClusterID string      `json:"cluster_id"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, "test_username", request.User.Username)
			assert.Equal(t, "9m4e2mr0ui3e8a215n4g", request.ClusterID)
			assert.Empty(t, request.User.Secrets)
			msg.Respond([]byte(`{}`))
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "workflow-definition",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	time.Sleep(100 * time.Millisecond)

	// Create request
	req, err := http.NewRequest("GET", "/kclusters/9m4e2mr0ui3e8a215n4g/resources", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/kclusters/{clusterID}/resources", getClusterResources)
	router.ServeHTTP(rr, req)
	wg.Wait()

	assert.Equal(t, http.StatusOK, rr.Code)
	assert.Equal(t, `{}`, rr.Body.String())
}

func testGetResourcesAuthError(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "", "test_password").
		Return("", nil)

	// Create request
	req, err := http.NewRequest("GET", "/kclusters/9m4e2mr0ui3e8a215n4g/resources", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = ""

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/kclusters/{clusterID}/resources", getClusterResources)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusUnauthorized, rr.Code)
	assert.NotEmpty(t, rr.Body.String())
	assert.Equal(t, `{"error": {"code": 401, "message": "unable to get info for authenticated user"}}`, rr.Body.String())
}

func testGetResourcesPermissionDenied(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "bad_username", "test_password2").
		Return("vault_token", nil)
	vaultClient.GetMock().On("ReadCluster", "bad_username", "9m4e2mr0ui3e8a215n4g", "vault_token").
		Return(common.Cluster{}, 403, fmt.Errorf("permission denied"))

	// Create request
	req, err := http.NewRequest("GET", "/kclusters/9m4e2mr0ui3e8a215n4g/resources", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password2")
	tokenStore["test_password2"] = "bad_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/kclusters/{clusterID}/resources", getClusterResources)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusForbidden, rr.Code)
	assert.Equal(t, `{"error": {"code": 403, "message": "unable to get kcluster: permission denied"}}`, rr.Body.String())
}

func testGetResourcesNATSError(t *testing.T) {
	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("ReadCluster", "test_username", "9m4e2mr0ui3e8a215n4g", "vault_token").
		Return(common.Cluster{
			ID:               "9m4e2mr0ui3e8a215n4g",
			Name:             "default",
			DefaultNamespace: "default",
			Config:           "config",
			Slug:             "default",
		}, 200, nil)

	req, err := http.NewRequest("GET", "/kclusters/9m4e2mr0ui3e8a215n4g/resources", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/kclusters/{clusterID}/resources", getClusterResources)
	router.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusInternalServerError, rr.Code)
	assert.Equal(t, `{"error": {"code": 500, "message": "unable to get kcluster resources: error connecting to NATS: nats: no servers available for connection"}}`, rr.Body.String())
}

func testGetResourcesInvalidNamespace(t *testing.T) {
	s := runNATSServer(t)
	defer s.Shutdown()

	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("ReadCluster", "test_username", "9m4e2mr0ui3e8a215n4g", "vault_token").
		Return(common.Cluster{
			ID:               "9m4e2mr0ui3e8a215n4g",
			Name:             "default",
			DefaultNamespace: "default",
			Config:           "config",
			Slug:             "default",
		}, 200, nil)

	var wg sync.WaitGroup
	wg.Add(1)
	go common.SynchronousTestSubscriber(
		"Phylax.GetResources",
		"Phylax.GetResourcesQueue",
		func(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
			var request struct {
				User      common.User `json:"user"`
				ClusterID string      `json:"cluster_id"`
				Namespace string      `json:"namespace"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, "test_username", request.User.Username)
			assert.Equal(t, "9m4e2mr0ui3e8a215n4g", request.ClusterID)
			assert.Equal(t, "test", request.Namespace)
			assert.Empty(t, request.User.Secrets)
			msg.Respond([]byte(`{"error": {"message": "unable to get pod list: pods is forbidden: User \"nafigos-user\" cannot list resource \"pods\" in API group \"\" in the namespace \"test\"", "code": 500}}`))
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "workflow-definition",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	time.Sleep(100 * time.Millisecond)

	// Create request
	req, err := http.NewRequest("GET", "/kclusters/9m4e2mr0ui3e8a215n4g/resources?namespace=test", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/kclusters/{clusterID}/resources", getClusterResources)
	router.ServeHTTP(rr, req)
	wg.Wait()

	assert.Equal(t, http.StatusInternalServerError, rr.Code)
	assert.Equal(t, `{"error": {"message": "unable to get pod list: pods is forbidden: User \"nafigos-user\" cannot list resource \"pods\" in API group \"\" in the namespace \"test\"", "code": 500}}`, rr.Body.String())
}

func testGetResourcesBadNATSResponse(t *testing.T) {
	s := runNATSServer(t)
	defer s.Shutdown()

	vaultClient = &MockVaultClient{new(mock.Mock), ""}
	vaultClient.GetMock().On("GetUserLoginToken", "test_username", "test_password").
		Return("vault_token", nil)
	vaultClient.GetMock().On("ReadCluster", "test_username", "9m4e2mr0ui3e8a215n4g", "vault_token").
		Return(common.Cluster{
			ID:               "9m4e2mr0ui3e8a215n4g",
			Name:             "default",
			DefaultNamespace: "default",
			Config:           "config",
			Slug:             "default",
		}, 200, nil)

	var wg sync.WaitGroup
	wg.Add(1)
	go common.SynchronousTestSubscriber(
		"Phylax.GetResources",
		"Phylax.GetResourcesQueue",
		func(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
			var request struct {
				User      common.User `json:"user"`
				ClusterID string      `json:"cluster_id"`
				Namespace string      `json:"namespace"`
			}
			err := json.Unmarshal(reqBytes, &request)
			assert.NoError(t, err)
			assert.Equal(t, "test_username", request.User.Username)
			assert.Equal(t, "9m4e2mr0ui3e8a215n4g", request.ClusterID)
			assert.Equal(t, "test", request.Namespace)
			assert.Empty(t, request.User.Secrets)
			msg.Respond([]byte(`{code: 500}`))
		},
		map[string]string{
			"cluster_id": "nafigos-cluster",
			"client_id":  "workflow-definition",
			"address":    "nats://localhost:4222",
		}, &wg,
	)
	time.Sleep(100 * time.Millisecond)

	// Create request
	req, err := http.NewRequest("GET", "/kclusters/9m4e2mr0ui3e8a215n4g/resources?namespace=test", nil)
	assert.NoError(t, err)
	req.Header.Add("Authorization", "test_password")
	tokenStore["test_password"] = "test_username"

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Use(AuthenticationMiddleware)
	router.HandleFunc("/kclusters/{clusterID}/resources", getClusterResources)
	router.ServeHTTP(rr, req)
	wg.Wait()

	assert.Equal(t, http.StatusInternalServerError, rr.Code)
	assert.Equal(t, `{"error": {"code": 500, "message": "unable to read response: invalid character 'c' looking for beginning of object key string"}}`, rr.Body.String())
}
