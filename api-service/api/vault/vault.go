// Package vault provides functions and structs for interacting with Vault for
// storing user secrets. It has functionality for both userpass and JWT auth
// methods
package vault

import (
	"fmt"
	"net/http"
	"regexp"
	"strconv"
	"strings"

	vault "github.com/hashicorp/vault/api"
)

const (
	namespace = "nafigos/"
)

// Client is a wrapper over vault.Client and includes information about the
// preferred auth method
type Client struct {
	*vault.Client
	AuthMethod string
}

// NewClient creates a new Vault client for interacting with the Vault server
func NewClient(address, token, authMethod string) (*Client, error) {
	config := vault.DefaultConfig()
	config.Address = address
	vClient, err := vault.NewClient(config)
	if err != nil {
		fmt.Println("Error creating client:", err)
		return nil, err
	}
	if len(authMethod) == 0 {
		authMethod = "userpass"
	}
	client := &Client{vClient, authMethod}
	client.SetToken(token)
	return client, nil
}

// GetAuthMethod returns the string identifying the method of auth for this client
func (client *Client) GetAuthMethod() string {
	return client.AuthMethod
}

func (client *Client) getUserClient(token string) (*vault.Client, error) {
	config := vault.DefaultConfig()
	config.Address = client.Address()
	userClient, err := vault.NewClient(config)
	if err != nil {
		return nil, err
	}
	userClient.SetToken(token)
	return userClient, nil
}

// CreateNewPolicy creates a policy allowing user to perform CRUD operations on their path
func (client *Client) CreateNewPolicy(owner string, isAdmin bool) (int, error) {
	policy := fmt.Sprintf(`path "%s%s/*" {capabilities = ["create", "read", "update", "delete", "list"]}`, namespace, owner)
	if !isAdmin {
		policy += fmt.Sprintf(` path "%s%s/info" {capabilities = ["create", "read", "update", "delete", "list"] denied_parameters = {"is_admin" = [true, "true"]}}`, namespace, owner)
	}

	err := client.Sys().PutPolicy(
		namespace+owner,
		policy,
	)
	if err != nil {
		return readVaultError(err.Error())
	}
	return http.StatusOK, nil
}

// CreateAdminPolicy ...
func (client *Client) CreateAdminPolicy() (int, error) {
	err := client.Sys().PutPolicy(
		namespace+"admin",
		fmt.Sprintf(`path "%s*" {capabilities = ["create", "read", "update", "delete", "list"]}`, namespace),
	)
	if err != nil {
		return readVaultError(err.Error())
	}
	return http.StatusOK, nil
}

// MountNewKVPath will create/mount the user's path and fail if it exists already
func (client *Client) MountNewKVPath(owner string) (int, error) {
	err := client.Sys().Mount(namespace+owner, &vault.MountInput{Type: "kv", Description: "Secrets for " + owner})
	if err != nil {
		return readVaultError(err.Error())
	}
	return http.StatusOK, nil
}

// EnableJWTAuth will enable the JWT authentication method
func (client *Client) EnableJWTAuth(oidcURL string) error {
	err := client.Sys().EnableAuthWithOptions("jwt", &vault.EnableAuthOptions{Type: "jwt"})
	if err != nil {
		return err
	}
	// Write config settings for JWT Auth
	_, err = client.Logical().Write("auth/jwt/config", map[string]interface{}{
		"oidc_discovery_url": oidcURL,
	})
	if err != nil {
		return err
	}
	return nil
}

// WriteJWTRole creates a JWT role for a user with access to their path using policy
func (client *Client) WriteJWTRole(username string, isAdmin bool) (int, error) {
	var adminPolicy string
	if isAdmin {
		adminPolicy = "," + namespace + "admin"
	}
	_, err := client.Logical().Write("auth/jwt/role/"+username, map[string]interface{}{
		"policies":        namespace + username + adminPolicy,
		"bound_audiences": "nafigos-client",
		"user_claim":      "preferred_username",
		"role_type":       "jwt",
	})
	if err != nil {
		return readVaultError(err.Error())
	}
	return http.StatusOK, nil
}

// EnableUserpassAuth will enable the userpass authentication method
func (client *Client) EnableUserpassAuth() error {
	err := client.Sys().EnableAuthWithOptions("userpass", &vault.EnableAuthOptions{Type: "userpass"})
	if err != nil {
		return err
	}
	return nil
}

// WriteUserpass creates a userpass for a user with access to their path using policy
func (client *Client) WriteUserpass(username, password string) (int, error) {
	_, err := client.Logical().Write("auth/userpass/users/"+username, map[string]interface{}{"password": password, "policies": namespace + username})
	if err != nil {
		return readVaultError(err.Error())
	}
	return http.StatusOK, nil
}

func (client *Client) write(path string, id string, data map[string]interface{}, token string) (int, error) {
	userClient, err := client.getUserClient(token)
	if err != nil {
		return readVaultError(err.Error())
	}
	_, err = userClient.Logical().Write(path+id, data)
	if err != nil {
		return readVaultError(err.Error())
	}
	return http.StatusOK, nil
}

func readVaultError(msg string) (code int, err error) {
	codeRE := regexp.MustCompile(`Code: (\d{3})`)
	codeResult := codeRE.FindStringSubmatch(msg)
	if len(codeResult) < 2 {
		err = fmt.Errorf("unable to find code in error message")
		return
	}
	// Ignore error since regexp makes it impossible
	code, _ = strconv.Atoi(codeResult[1])
	msgRE := regexp.MustCompile(`\* (.+)`)
	errMatches := msgRE.FindAllStringSubmatch(msg, -1)
	if len(errMatches) == 0 {
		err = fmt.Errorf("unable to find specific message in error")
		return
	}
	errs := []string{}
	for _, match := range errMatches {
		if len(match) > 1 {
			errs = append(errs, match[1])
		}
	}
	err = fmt.Errorf(strings.Join(errs, "\n"))
	return
}
