package vault

import (
	"gitlab.com/cyverse/nafigos/common"

	"github.com/stretchr/testify/mock"
)

// Adapter is an interface that allows for mocking Vault client
type Adapter interface {
	AddSecret(string, string, map[string]interface{}, string) (int, error)
	EditSecret(string, string, common.Secret, string) (int, error)
	DeleteSecret(string, string, string) (int, error)
	ReadSecret(string, string, string) (common.Secret, int, error)
	GetAllSecrets(string, string) (map[string]common.Secret, int, error)
	GetUserLoginToken(string, string) (string, error)
	WriteUser(common.User, string) (int, error)
	DeleteUser(string) (int, error)
	ReadUser(string, string) (common.User, int, error)
	ListAllUsers() ([]string, int, error)
	AddCluster(string, string, common.Cluster, string) (int, error)
	ReadCluster(string, string, string) (common.Cluster, int, error)
	EditCluster(string, string, common.Cluster, string) (int, error)
	DeleteCluster(string, string, string) (int, error)
	GetAllClusters(string, string) (map[string]common.Cluster, int, error)
	CreateNewPolicy(string, bool) (int, error)
	CreateAdminPolicy() (int, error)
	MountNewKVPath(string) (int, error)
	EnableJWTAuth(string) error
	WriteJWTRole(string, bool) (int, error)
	EnableUserpassAuth() error
	WriteUserpass(string, string) (int, error)
	GetAuthMethod() string
	GetMock() *mock.Mock
}

// GetMock ...
func (client *Client) GetMock() (m *mock.Mock) {
	return
}
