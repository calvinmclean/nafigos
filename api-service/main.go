package main

import (
	"net/http"
	"os"

	"gitlab.com/cyverse/nafigos/api-service/api"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

const (
	defaultNATSClusterID       = "nafigos-cluster"
	defaultNATSClientID        = "api"
	defaultNATSConnAddress     = "nats://localhost:4222"
	defaultVaultAddress        = "http://vault:8200"
	defaultKeycloakURL         = "http://keycloak:8080/auth/realms/nafigos"
	defaultKeycloakRedirectURL = "http://api:8080/user/login/callback"
	defaultKeycloakClientID    = "nafigos-client"
)

func init() {
	log.SetFormatter(&log.JSONFormatter{})
	log.SetOutput(os.Stdout)
	level, err := log.ParseLevel(os.Getenv("LOG_LEVEL"))
	if err != nil {
		level = log.InfoLevel
	}
	log.SetLevel(level)

	logger := log.WithFields(log.Fields{
		"package":  "main",
		"function": "init",
	})

	api.InitNATSConnectionVars(defaultNATSClusterID, defaultNATSClientID, defaultNATSConnAddress)
	logger.Info("initialized NATS connections variables")

	if os.Getenv("AUTH_METHOD") == "keycloak" {
		err = api.InitKeycloakClient(
			defaultKeycloakURL,
			defaultKeycloakRedirectURL,
			defaultKeycloakClientID,
		)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Panic("unable to connect to Keycloak")
			panic(err)
		}
		logger.Info("initialized connection to Keycloak")
	}

	err = api.InitVaultClient(defaultVaultAddress)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Panic("unable to connect to Vault")
		panic(err)
	}
	logger.Info("initialized connection to Vault")
}

func main() {
	// Router is only used for User things since they won't require authentication
	router := mux.NewRouter().StrictSlash(true)
	api.AuthAPIRouter(router)

	// Require Admin permission to access
	api.AdminAPIRouter(router)

	// Subrouter will be used for all paths other than login since it will
	// require headers to authenticate with AuthenticateMiddleware
	subRouter := router.NewRoute().Subrouter()
	subRouter.Use(api.AuthenticationMiddleware)
	api.UserAPIRouter(subRouter)
	api.KclusterAPIRouter(subRouter)
	api.WorkflowDefinitionAPIRouter(subRouter)
	api.BuildAPIRouter(subRouter)
	//Put new routes above this line
	//RunAPIRouter defines a "catchall" endpoint for HTTP scaling
	api.RunAPIRouter(subRouter)

	log.WithFields(log.Fields{
		"package":  "main",
		"function": "init",
	}).Info("API Service listening on port 8080")
	http.ListenAndServe(":8080", router)
}
