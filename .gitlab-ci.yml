image: "golang:1.13-stretch"

stages:
  - dependencies
  - format
  - lint
  - test
  - release

variables:
  NAFIGOS_ROOT: /go/src/gitlab.com/cyverse/nafigos

before_script:
  - mkdir -p /go/src/gitlab.com/cyverse
  - cp -r $CI_PROJECT_DIR $NAFIGOS_ROOT

dep_check:
  stage: dependencies
  except:
    - master
  script:
    - apt-get install curl
    - curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh
    - cd $NAFIGOS_ROOT
    - dep check

# Note: gofmt will recurse to subpackages
.format:
  stage: format
  needs: []
  except:
    - master
  variables:
    GOFMT: gofmt -s -l -d
  script:
    - cd $NAFIGOS_ROOT
    - $GOFMT ./$SERVICE | tee output.txt
    - test $(cat output.txt | wc -l) -eq 0
format_api:
  variables:
    SERVICE: api-service
  extends: .format
format_build:
  variables:
    SERVICE: build-service
  extends: .format
format_cmd:
  variables:
    SERVICE: cmd
  extends: .format
format_common:
  variables:
    SERVICE: common
  extends: .format
format_pleo:
  variables:
    SERVICE: pleo
  extends: .format
format_workflow_definition:
  variables:
    SERVICE: workflow-definition-service
  extends: .format
format_phylax:
  variables:
    SERVICE: phylax-service
  extends: .format

# Note: go lint will **not** recurse to subpackages
.lint:
  stage: lint
  needs: []
  except:
    - master
  before_script:
    - go get golang.org/x/lint/golint
    - go get github.com/gordonklaus/ineffassign
    - go install github.com/gordonklaus/ineffassign
    - go get github.com/client9/misspell/cmd/misspell
    - mkdir -p /go/src/gitlab.com/cyverse
    - cp -r $CI_PROJECT_DIR $NAFIGOS_ROOT
  script:
    - cd $NAFIGOS_ROOT
    - for dir in $(go list ./$SERVICE/...); do golint $dir; done | tee output.txt
    - test $(cat output.txt | wc -l) -eq 0
    - ineffassign ./$SERVICE
    - misspell ./$SERVICE
lint_api:
  variables:
    SERVICE: api-service
  extends: .lint
  needs: [format_api]
lint_build:
  variables:
    SERVICE: build-service
  extends: .lint
  needs: [format_build]
lint_cmd:
  variables:
    SERVICE: cmd
  extends: .lint
  needs: [format_cmd]
lint_common:
  variables:
    SERVICE: common
  extends: .lint
  needs: [format_common]
lint_pleo:
  variables:
    SERVICE: pleo
  extends: .lint
  needs: [format_pleo]
lint_workflow_definition:
  variables:
    SERVICE: workflow-definition-service
  extends: .lint
  needs: [format_workflow_definition]
lint_phylax:
  variables:
    SERVICE: phylax-service
  extends: .lint
  needs: [format_phylax]

.test:
  stage: test
  needs: []
  except:
    - master
vault_test:
  script:
    - cd $NAFIGOS_ROOT/api-service/api/vault
    - go get github.com/hashicorp/vault/command
    - rm -rf /go/src/github.com/hashicorp/vault/vendor/golang.org/x/net/trace
    - rm -rf /go/src/gitlab.com/cyverse/nafigos/vendor/golang.org/x/net/trace
    - go get golang.org/x/net/trace
    - go test -v -covermode=count
  extends: .test
  needs: [lint_api]
api_service_test:
  script:
    - cd $NAFIGOS_ROOT/api-service/api
    - go get github.com/nats-io/nats-streaming-server/server
    - go test -v -covermode=count
  extends: .test
  needs: [lint_api]
workflow_definition_service_test:
  script:
    - go get github.com/nats-io/nats-streaming-server/server
    - cd $NAFIGOS_ROOT/workflow-definition-service/workflowdefinition
    - go test -v -covermode=count
    - cd types
    - go test -v -covermode=count
  extends: .test
  needs: [lint_workflow_definition]
pleo_test:
  script:
    - cd $NAFIGOS_ROOT/pleo/util
    - go test -v -covermode=count
  extends: .test
  needs: [lint_pleo]
build_service_test:
  script:
    - go get github.com/nats-io/nats-streaming-server/server
    - cd $NAFIGOS_ROOT/build-service/build
    - go test -v -covermode=count
  extends: .test
  needs: [lint_build]
phylax_service_test:
  script:
    - cd $NAFIGOS_ROOT/phylax-service/phylax
    - go test -v -covermode=count
  extends: .test
  needs: [lint_phylax]
common_test:
  script:
    - cd $NAFIGOS_ROOT/common
    - go test -v -covermode=count
  extends: .test
  needs: [lint_common]
# Not really a test per se but it's good to ensure that the CLI will at
# least build
cli_test:
  script:
    - cd $NAFIGOS_ROOT/cmd
    - go build -o nafigos
  extends: .test

.docker_release:
  image: docker:latest
  stage: release
  only:
    - master
  services:
    - docker:dind
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -f $DOCKERFILE -t $IMAGE_RELEASE_TAG .
    - docker push $IMAGE_RELEASE_TAG
api_service_release:
  variables:
    IMAGE_RELEASE_TAG: $CI_REGISTRY_IMAGE/api_service:latest
    DOCKERFILE: ./api-service/Dockerfile
  extends: .docker_release
build_service_release:
  variables:
    IMAGE_RELEASE_TAG: $CI_REGISTRY_IMAGE/build_service:latest
    DOCKERFILE: ./build-service/Dockerfile
  extends: .docker_release
pleo_release:
  variables:
    IMAGE_RELEASE_TAG: $CI_REGISTRY_IMAGE/pleo:latest
    DOCKERFILE: ./pleo/Dockerfile
  extends: .docker_release
phylax_release:
  variables:
    IMAGE_RELEASE_TAG: $CI_REGISTRY_IMAGE/phylax:latest
    DOCKERFILE: ./phylax-service/Dockerfile
  extends: .docker_release
workflow_definition_service_release:
  variables:
    IMAGE_RELEASE_TAG: $CI_REGISTRY_IMAGE/workflow_definition_service:latest
    DOCKERFILE: ./workflow-definition-service/Dockerfile
  extends: .docker_release
