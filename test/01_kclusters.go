package test

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"testing"

	"gitlab.com/cyverse/nafigos/common"

	"github.com/stretchr/testify/assert"
)

var clusterID string
var generatedCluster common.Cluster

var tests01Kcluster = []testMap{
	{"CreateKcluster": testCreateKcluster},
	{"GetKcluster": testGetKcluster},
	{"GetAllKclusters": testGetAllKclusters},
	{"UpdateKcluster": testUpdateKcluster},
	{"DeleteKcluster": testDeleteKcluster},
	{"GetKclusterNotFound": testGetKclusterNotFound},
}

func testCreateKcluster(t *testing.T) {
	data := `{
	"config": "my-kubeconfig",
	"name": "my-cluster",
	"host": "my-host",
	"default_namespace": "my-namespace"
}`
	req, err := newRequest("POST", "http://localhost/kclusters", data, userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusCreated, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	var cluster common.Cluster
	err = json.Unmarshal(respBody, &cluster)
	assert.NoError(t, err)
	clusterID = cluster.ID
	assert.Len(t, clusterID, 20)
	assert.Equal(t, "my-kubeconfig", cluster.Config)
	assert.Equal(t, "my-cluster", cluster.Name)
	assert.Equal(t, "my-host", cluster.Host)
	assert.Equal(t, "my-namespace", cluster.DefaultNamespace)
}

func testGetKcluster(t *testing.T) {
	req, err := newRequest("GET", "http://localhost/kclusters/"+clusterID, "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	var cluster common.Cluster
	err = json.Unmarshal(respBody, &cluster)
	assert.NoError(t, err)
	assert.Equal(t, "my-kubeconfig", cluster.Config)
	assert.Equal(t, "my-cluster", cluster.Name)
	assert.Equal(t, "my-host", cluster.Host)
	assert.Equal(t, "my-namespace", cluster.DefaultNamespace)
	assert.Equal(t, clusterID, cluster.ID)
}

func testGetAllKclusters(t *testing.T) {
	req, err := newRequest("GET", "http://localhost/kclusters", "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	var clusters map[string]common.Cluster
	err = json.Unmarshal(respBody, &clusters)
	assert.NoError(t, err)
	assert.Equal(t, 2, len(clusters))
	assert.NotEmpty(t, clusters[clusterID])
}

func testUpdateKcluster(t *testing.T) {
	data := `{
	"config": "new-kubeconfig",
	"name": "new-name",
	"host": "new-host",
	"default_namespace": "new-namespace"
}`
	req, err := newRequest("PUT", "http://localhost/kclusters/"+clusterID, data, userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusCreated, resp.StatusCode)

	req, err = newRequest("GET", "http://localhost/kclusters/"+clusterID, "", userToken)
	assert.NoError(t, err)
	resp, err = (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	var cluster common.Cluster
	err = json.Unmarshal(respBody, &cluster)
	assert.NoError(t, err)
	assert.Equal(t, "new-kubeconfig", cluster.Config)
	assert.Equal(t, "new-name", cluster.Name)
	assert.Equal(t, "new-host", cluster.Host)
	assert.Equal(t, "new-namespace", cluster.DefaultNamespace)
	assert.Equal(t, clusterID, cluster.ID)
}

func testDeleteKcluster(t *testing.T) {
	req, err := newRequest("DELETE", "http://localhost/kclusters/"+clusterID, "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusNoContent, resp.StatusCode)
}

func testGetKclusterNotFound(t *testing.T) {
	req, err := newRequest("GET", "http://localhost/kclusters/"+clusterID, "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusBadRequest, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Equal(t, fmt.Sprintf(`{"error": {"code": 400, "message": "unable to get kcluster: cluster '%s' not found"}}`, clusterID), string(respBody))
}
