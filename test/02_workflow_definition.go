package test

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"testing"
	"time"

	"gitlab.com/cyverse/nafigos/workflow-definition-service/workflowdefinition"

	"github.com/stretchr/testify/assert"
)

var workflowID, adminWorkflowID, errorWorkflowID string

var tests02WorkflowDefinition = []testMap{
	{"CreateWorkflowDefinition": testCreateWorkflowDefinition},
	{"CreateWorkflowDefinitionBadURL": testCreateWorkflowDefinitionBadURL},
	{"CreateWorkflowDefinitionForAdmin": testCreateWorkflowDefinitionForAdmin},
	{"GetWorkflowDefinition": testGetWorkflowDefinition},
	{"GetWorkflowDefinitionPermissionDenied": testGetWorkflowDefinitionPermissionDenied},
	{"GetAllWorkflowDefinitions": testGetAllWorkflowDefinitions},
	// {"UpdateWorkflowDefinition": testUpdateWorkflowDefinition}, // Need a local git repo for testing to make this work
}

func testCreateWorkflowDefinition(t *testing.T) {
	data := `{"url": "https://gitlab.com/cyverse/nafigos-helloworld", "branch": "master"}`
	req, err := newRequest("POST", "http://localhost/workflows", data, userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	var result map[string]string
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	workflowID = result["id"]
	assert.Len(t, workflowID, 20)
	time.Sleep(2 * time.Second)
}

func testCreateWorkflowDefinitionBadURL(t *testing.T) {
	data := `{"url": "https://bad-url.com/bad-git-user/bad-git-repo", "branch": "master"}`
	req, err := newRequest("POST", "http://localhost/workflows", data, userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	var result map[string]string
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	assert.Len(t, result["id"], 20)
	time.Sleep(2 * time.Second)
	errorWorkflowID = result["id"]

	// Since WFD is created asynchronously, I will have to get it to see error
	req, err = newRequest("GET", "http://localhost/workflows/"+errorWorkflowID, "", userToken)
	assert.NoError(t, err)
	resp, err = (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	respBody, err = ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	var getResult struct {
		Data workflowdefinition.WorkflowDefinition `json:"data"`
	}
	err = json.Unmarshal(respBody, &getResult)
	assert.NoError(t, err)
	assert.Equal(t, errorWorkflowID, getResult.Data.ID)
	assert.Regexp(t, regexp.MustCompile(`correct error in git and update: Get \"https://bad-url\.com\/bad-git-user\/bad-git-repo\/info\/refs\?service=git-upload-pack\":.+`), getResult.Data.Error)
}

func testCreateWorkflowDefinitionForAdmin(t *testing.T) {
	data := `{"url": "https://gitlab.com/cyverse/nafigos-helloworld", "branch": "master"}`
	req, err := newRequest("POST", "http://localhost/workflows", data, adminToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	var result map[string]string
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	adminWorkflowID = result["id"]
	assert.Len(t, workflowID, 20)
	time.Sleep(1 * time.Second)
}

func testGetWorkflowDefinition(t *testing.T) {
	req, err := newRequest("GET", "http://localhost/workflows/"+workflowID, "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	var result struct {
		Data workflowdefinition.WorkflowDefinition `json:"data"`
	}
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	assert.Equal(t, workflowID, result.Data.ID)
}

func testGetWorkflowDefinitionPermissionDenied(t *testing.T) {
	req, err := newRequest("GET", "http://localhost/workflows/"+adminWorkflowID, "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusInternalServerError, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	assert.Equal(t, fmt.Sprintf(`{"error":{"message":"unable to get WorkflowDefinition '%s': user not authorized for this WorkflowDefinition","code":500},"data":{}}`, adminWorkflowID), string(respBody))
}

func testGetAllWorkflowDefinitions(t *testing.T) {
	req, err := newRequest("GET", "http://localhost/workflows", "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	var result struct {
		Data []workflowdefinition.WorkflowDefinition `json:"data"`
	}
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	assert.Equal(t, 2, len(result.Data))
	assert.Equal(t, workflowID, result.Data[0].ID)
}

func testUpdateWorkflowDefinition(t *testing.T) {}

func testDeleteWorkflowDefinition(t *testing.T) {
	req, err := newRequest("DELETE", "http://localhost/workflows/"+workflowID, "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusNoContent, resp.StatusCode)
}

func testDeleteErrorWorkflowDefinition(t *testing.T) {
	req, err := newRequest("DELETE", "http://localhost/workflows/"+errorWorkflowID, "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusNoContent, resp.StatusCode)
}

func testDeleteAdminWorkflowDefinition(t *testing.T) {
	req, err := newRequest("DELETE", "http://localhost/workflows/"+adminWorkflowID, "", adminToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusNoContent, resp.StatusCode)
}
