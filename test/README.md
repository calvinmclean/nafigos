# Integration Testing

### Table of Contents
[[_TOC_]]


## Running the tests

You will need to run the ansible install with the setup expected for local development and Skaffold, with the addition of this variable in the `config.yml`:
```yaml
KEYCLOAK_USERS:
  - username: nafigos-user
    password: nafigos-user-password
    admin: false
  - username: nafigos-admin
    password: nafigos-admin-password
    admin: true
```

Then, run `go test -v` from this directory.


## Important Notes
- Everything is cleaned up at the end of the tests except for `Runners` in MongoDB

- The Build Service is currently not tested because it requires credentials for a container registry

- Since `WorkflowDefinitions` rely on git repositories, these tests could all be broken by a change to the `gitlab.com/cyverse/nafigos-helloworld` repository

- `ScaleFromZero` functionality is currently untested because the retry requests will use the `Host` header of the incoming request rather than the original IP/domain being used


## About the tests
The tests are organized in files to group by what functionality they test. These files are named according to the order that they are run. Since order is important for these tests, each file has a slice near the top laying out the order of the tests in that file. Finally, all the slices are combined in `integration_test.go` and run in order. Then, it completes by deleting the namespace created for the test user, which will clean up all remaining resources in that namespace (this may take awhile).

Some variables are shared between test functions so that we can save the `workflowID` of a created `WorkflowDefinition` in order to use it for Runs and Builds.


## Adding to the tests
When adding tests, make sure to put your new test in the slice in the correct order. Make sure to be careful with variables defined at the top of the file before overwriting them because this could break other tests.

Some tests are defined in the corresponding files, but not included in the slice because they are instead added to the `cleanupTests` slice in `integration_test.go` to be run after all other tests. This is useful for deleting resources such as `WorkflowDefinitions` that may be needed by other tests.
