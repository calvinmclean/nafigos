package command

import (
	"fmt"
	"strings"
)

// GetSecretCommand ...
type GetSecretCommand struct {
	*BaseCommand
}

// Run ...
func (c *GetSecretCommand) Run(args []string) int {
	if len(args) < 1 {
		c.UI.Error("Incorrect number of arguments (expected >1)")
		c.UI.Error(c.Help())
		return 1
	}

	username := args[0]
	var secretID string
	if len(args) > 1 {
		secretID = "/" + args[1]
	}

	req := c.NewRequest("GET", fmt.Sprintf("/users/%s/secrets%s", username, secretID))
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *GetSecretCommand) Synopsis() string {
	return "get secrets"
}

// Help ...
func (c *GetSecretCommand) Help() string {
	helpText := `
Usage: nafigos get secret USERNAME [ID]

	Gets all secrets unless specific ID is provided
`
	return strings.TrimSpace(helpText)
}
