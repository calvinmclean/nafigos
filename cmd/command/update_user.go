package command

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"strings"

	"gitlab.com/cyverse/nafigos/common"
)

// UpdateUserCommand ...
type UpdateUserCommand struct {
	*BaseCommand
}

// Run ...
func (c *UpdateUserCommand) Run(args []string) int {
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	filename := flagSet.String("f", "", "filename to use")
	admin := flagSet.Bool("admin", false, "admin privileges")
	flagSet.Parse(args)
	args = flagSet.Args()

	c.PrintDebug("Parsed command line flags:\n  filename: '%s'\n  admin: %t\n", *filename, *admin)

	if len(args) == 1 && args[0] == "help" {
		c.UI.Error(c.Help())
		return 0
	}

	if len(args) != 1 {
		c.UI.Error("Incorrect number of positional arguments (expected 1)")
		c.UI.Error(c.Help())
		return 1
	}
	targetUsername := args[0]

	var data []byte
	var err error
	if len(*filename) > 0 {
		data, err = ioutil.ReadFile(*filename)
		if err != nil {
			panic(err)
		}

		c.PrintDebug("Successfully read file '%s':\n%s", *filename, string(data))

		if *admin {
			data = c.override(data, admin)
			c.PrintDebug("Successfully overrode parameters in file:\n%s\n", string(data))
		}
	} else {
		data = []byte(fmt.Sprintf(`{
	"is_admin": %t
}`, *admin))

		c.PrintDebug("Constructed request body:\n%s\n", string(data))
	}

	req := c.NewRequest("PUT", "/users/"+targetUsername)
	req.Body = ioutil.NopCloser(bytes.NewReader(data))
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *UpdateUserCommand) Synopsis() string {
	return "update a user"
}

// Help ...
func (c *UpdateUserCommand) Help() string {
	helpText := `
	Usage: nafigos update user [options] USERNAME

	Options:
		-f <filename>
			name of JSON file to read secret info from
		-admin
			update user with admin permissions
`
	return strings.TrimSpace(helpText)
}

func (c *UpdateUserCommand) override(data []byte, admin *bool) []byte {
	var userJSON common.User
	json.Unmarshal(data, &userJSON)

	if *admin {
		userJSON.IsAdmin = true
	}

	data, err := json.Marshal(userJSON)
	if err != nil {
		panic(err)
	}
	return data
}
