package command

import (
	"net/http"
	"os"
)

// BaseCommand ...
type BaseCommand struct {
	Meta
	nafigosAPI string
	authToken  string
}

// NewBaseCommand ...
func NewBaseCommand(meta Meta) (c BaseCommand) {
	c.Meta = meta
	c.authToken = os.Getenv("NAFIGOS_TOKEN")
	c.nafigosAPI = os.Getenv("NAFIGOS_API")
	// TODO: Fix this if string is empty
	if len(c.nafigosAPI) > 4 && c.nafigosAPI[0:4] != "http" {
		c.nafigosAPI = "http://" + c.nafigosAPI
	}
	return
}

// Client returns an HTTP Client to be used by the CLI
func (c *BaseCommand) Client() *http.Client {
	return &http.Client{}
}

// DoRequest will either execute a Request or print the curl command
func (c *BaseCommand) DoRequest(req *http.Request) {
	c.PrintDebug("Request struct:\n%v\n", req)
	if c.OutputCurlString {
		c.PrintCurlString(req)
	} else {
		resp, err := c.Client().Do(req)
		if err != nil {
			panic(err)
		}
		c.PrintDebug("Response struct:\n%v\n", resp)
		c.PrintHTTPResponse(resp)
	}
}

// NewRequest creates a new HTTP request with the correct header
func (c *BaseCommand) NewRequest(verb, path string) *http.Request {
	req, err := http.NewRequest(verb, c.nafigosAPI+path, nil)
	if err != nil {
		panic(err)
	}
	req.Header.Add("Authorization", c.authToken)
	return req
}
