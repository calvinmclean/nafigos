package command

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"strings"

	"gitlab.com/cyverse/nafigos/common"
)

// CreateSecretCommand ...
type CreateSecretCommand struct {
	*BaseCommand
}

// Run ...
func (c *CreateSecretCommand) Run(args []string) int {
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	filename := flagSet.String("f", "", "filename to use")
	secretType := flagSet.String("type", "", "type of secret (gcr, dockerhub, or git)")
	username := flagSet.String("username", "", "username to use with secret")
	value := flagSet.String("value", "", "value of the secret")
	flagSet.Parse(args)
	args = flagSet.Args()

	c.PrintDebug("Parsed command line flags:\n  filename: '%s'\n  secretType: '%s'\n  username: '%s'\n  value: '%s'\n", *filename, *secretType, *username, *value)

	if len(args) < 1 {
		c.UI.Error("Incorrect number of positional arguments (expected 1)")
		c.UI.Error(c.Help())
		return 1
	}
	if args[0] == "help" {
		c.UI.Error(c.Help())
		return 1
	}

	var data []byte
	var err error
	if len(*filename) > 0 {
		data, err = ioutil.ReadFile(*filename)
		if err != nil {
			panic(err)
		}

		c.PrintDebug("Successfully read file '%s':\n%s", *filename, string(data))

		if len(*secretType) > 0 || len(*username) > 0 || len(*value) > 0 {
			data = c.override(data, secretType, username, value)
			c.PrintDebug("Successfully overrode parameters in file:\n%s\n", string(data))
		}
	} else {
		data = []byte(fmt.Sprintf(`{
	"type": "%s",
	"username": "%s",
	"value": "%s"
}`, *secretType, *username, *value))

		c.PrintDebug("Constructed request body:\n%s\n", string(data))
	}

	req := c.NewRequest("POST", "/users/"+args[0]+"/secrets")
	req.Body = ioutil.NopCloser(bytes.NewReader(data))
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *CreateSecretCommand) Synopsis() string {
	return "create a new secret for user"
}

// Help ...
func (c *CreateSecretCommand) Help() string {
	helpText := `
Usage: nafigos create secret [options] USERNAME

	Of the following options, it is required that you use either '-f <filename>' 
	or utilize all three other options

Options:
	-f <filename>
  		name of json file to read secret from
	-type <string>
		type of the secret (git, dockerhub, or gcr)
		(required if -f is not used)
	-value <string>
		value of the secret (often a personal access token)
		(required if -f is not used)
	-username <string>
		username associated with the secret
		(required if -f is not used)
`
	return strings.TrimSpace(helpText)
}

func (c *CreateSecretCommand) override(data []byte, secretType *string, username *string, value *string) []byte {
	var secretJSON common.Secret
	json.Unmarshal(data, &secretJSON)

	if len(*secretType) > 0 {
		secretJSON.Type = common.SecretType(*secretType)
	}
	if len(*username) > 0 {
		secretJSON.Username = *username
	}
	if len(*value) > 0 {
		secretJSON.Value = *value
	}

	data, err := json.Marshal(secretJSON)
	if err != nil {
		panic(err)
	}
	return data
}
