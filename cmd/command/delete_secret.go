package command

import (
	"fmt"
	"strings"
)

// DeleteSecretCommand ...
type DeleteSecretCommand struct {
	*BaseCommand
}

// Run ...
func (c *DeleteSecretCommand) Run(args []string) int {
	if len(args) != 2 {
		c.UI.Error("Incorrect number of arguments (expected 2)")
		c.UI.Error(c.Help())
		return 1
	}

	username := args[0]
	secretID := args[1]

	req := c.NewRequest("DELETE", fmt.Sprintf("/users/%s/secrets/%s", username, secretID))
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *DeleteSecretCommand) Synopsis() string {
	return "delete a secret"
}

// Help ...
func (c *DeleteSecretCommand) Help() string {
	helpText := `
Usage: nafigos delete secret USERNAME [secret ID]

Run 'nafigos get secret USERNAME' to view secret ID's for a user.
`
	return strings.TrimSpace(helpText)
}
