package main

import (
	"fmt"
	"os"

	"github.com/mitchellh/cli"
	"gitlab.com/cyverse/nafigos/cmd/command"
)

func checkRequirements() error {
	if len(os.Getenv("NAFIGOS_TOKEN")) == 0 {
		return fmt.Errorf("NAFIGOS_TOKEN env var is required")
	}
	if len(os.Getenv("NAFIGOS_API")) == 0 {
		return fmt.Errorf("NAFIGOS_API env var is required")
	}
	return nil
}

// Run ...
func Run(args []string) int {
	// Check for universal flags
	output := "default_pretty"
	outputCurlString := false
	debug := false
	for i, arg := range args {
		if (arg == "-o" || arg == "-output") && len(args) > i+1 {
			if args[i+1] == "raw" {
				output = "raw"
			}
			args[i] = ""
			args[i+1] = ""
		} else if arg == "-output-curl-string" {
			outputCurlString = true
			args[i] = ""
		} else if arg == "-debug" {
			debug = true
			args[i] = ""
		}
	}

	// Meta-option for executables.
	// It defines output color and its stdout/stderr stream.
	meta := &command.Meta{
		UI: &cli.ColoredUi{
			InfoColor:  cli.UiColorBlue,
			ErrorColor: cli.UiColorRed,
			Ui: &cli.BasicUi{
				Writer:      os.Stdout,
				ErrorWriter: os.Stderr,
				Reader:      os.Stdin,
			},
		},
		OutputFormat:     output,
		OutputCurlString: outputCurlString,
		Debug:            debug,
	}

	// Check if 'help' or '--help' is in args
	help := false
	for _, arg := range args {
		if arg == "help" || arg == "--help" {
			help = true
			break
		}
	}

	// Do not worry about requirements if requesting help
	err := checkRequirements()
	if !help && err != nil {
		meta.UI.Error(err.Error())
		return 1
	}

	return RunCustom(args, Commands(meta))
}

// RunCustom ...
func RunCustom(args []string, commands map[string]cli.CommandFactory) int {
	// Get the command line args. We shortcut "--version" and "-v" to
	// just show the version.
	for _, arg := range args {
		if arg == "-v" || arg == "-version" || arg == "--version" {
			newArgs := make([]string, len(args)+1)
			newArgs[0] = "version"
			copy(newArgs[1:], args)
			args = newArgs
			break
		}
	}

	cli := &cli.CLI{
		Args:     args,
		Commands: commands,
		Version:  Version,
		HelpFunc: func(cmds map[string]cli.CommandFactory) string {
			additionalOutput := `
Universal flags:
    -debug                      enable extra output when running commands
    -output-curl-string         instead of executing the command, print the equivalent curl command
    -o -output <output_type>    control the output format of HTTP responses options: [raw,default_pretty]
`
			return cli.BasicHelpFunc(Name)(cmds) + additionalOutput
		},
		HelpWriter: os.Stdout,
	}

	exitCode, err := cli.Run()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to execute: %s\n", err.Error())
	}

	return exitCode
}
