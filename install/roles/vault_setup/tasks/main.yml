---
###
# Preliminary setup steps
###
- name: FILE; create config directory
  file:
    path: "{{ CONFIG_DIR }}"
    state: directory

- name: PACKAGE; install dependencies
  package:
    name:
      - unzip
      - python-pip
      - postgresql-server-dev-all
    state: present

- name: PIP; install psycopg2 for PostgreSQL interactions
  pip:
    name: psycopg2

- name: UFW; enable access to SSH port
  ufw:
    state: enabled
    rule: allow
    port: ssh

- name: UFW; enable access to Vault port
  ufw:
    state: enabled
    rule: allow
    port: "8200"
    from_ip: "{{ UFW_FROM_IP }}"

- name: POSTGRESQL_TABLE; create 'vault_kv_store' table
  postgresql_table:
    db: "{{ POSTGRES_DB_NAME }}"
    login_user: "{{ POSTGRES_USERNAME }}"
    login_password: "{{ POSTGRES_PASSWORD }}"
    login_host: "{{ POSTGRES_HOST }}"
    owner: "{{ POSTGRES_USERNAME }}"
    port: "{{ POSTGRES_PORT }}"
    table: "vault_kv_store"
    columns:
    - parent_path TEXT COLLATE "C" NOT NULL
    - path TEXT COLLATE "C"
    - key TEXT COLLATE "C"
    - value BYTEA
    - CONSTRAINT pkey PRIMARY KEY (path, key)

- name: POSTGRESQL_IDX; create 'parent_path_idx' index
  postgresql_idx:
    db: "{{ POSTGRES_DB_NAME }}"
    login_user: "{{ POSTGRES_USERNAME }}"
    login_password: "{{ POSTGRES_PASSWORD }}"
    login_host: "{{ POSTGRES_HOST }}"
    port: "{{ POSTGRES_PORT }}"
    table: "vault_kv_store"
    idxname: "parent_path_idx"
    columns:
      - "parent_path"

###
# Install Vault binary
###
- name: GET_URL; download Vault binary
  get_url:
    url: "https://releases.hashicorp.com/vault/{{ VAULT_VERSION }}/vault_{{ VAULT_VERSION }}_linux_amd64.zip"
    dest: /tmp/vault.zip

- name: UNARCHIVE; unzip the Vault binary
  unarchive:
    src: /tmp/vault.zip
    dest: /usr/local/bin
    mode: 0755
    remote_src: yes

###
# Prepare Vault files and docker-compose
###
- name: COPY; create cert file
  copy:
    content: "{{ VAULT_TLS_CERTDATA }}"
    dest: "{{ VAULT_TLS_CERTFILE }}"
  when:
    - not VAULT_TLS_DISABLE
    - VAULT_TLS_CERTDATA is defined

- name: COPY; create key file
  copy:
    content: "{{ VAULT_TLS_KEYDATA }}"
    dest: "{{ VAULT_TLS_KEYFILE }}"
  when:
    - not VAULT_TLS_DISABLE
    - VAULT_TLS_KEYDATA is defined

- name: TEMPLATE; copy over Vault docker-compose file
  template:
    src: "docker-compose.yml.j2"
    dest: "{{ CONFIG_DIR }}/docker-compose.yml"

- name: TEMPLATE; copy over Vault systemd file
  template:
    src: "vault.service.j2"
    dest: "{{ CONFIG_DIR }}/vault.service"

- name: FILE; create symlink from systemd file to correct directory
  file:
    src: "{{ CONFIG_DIR }}/vault.service"
    dest: "/etc/systemd/system/vault.service"
    state: link

- name: SYSTEMD; enable and start vault service
  systemd:
    name: vault.service
    daemon_reload: yes
    enabled: yes
    state: restarted

###
# Unseal Vault
###
- name: SET_FACT; set VAULT_ADDR fact
  set_fact:
    VAULT_ADDR: "{{ VAULT_TLS_DISABLE | ternary('http', 'https') }}://localhost:{{ VAULT_PORT }}"

- name: GET_URI; check vault status
  uri:
    url: "{{ VAULT_ADDR }}/v1/sys/seal-status"
    method: GET
    validate_certs: false
  register: vault_status

- name: VAULT; vault status
  debug:
    msg:
    - "Vault Initialized: {{ vault_status.json.initialized }}"
    - "Vault Sealed: {{ vault_status.json.sealed }}"

- name: VAULT; init Vault if not already initialized
  block:
  # Using `uri` here instead of `vault` command because the JSON response is easier to handle
  - name: URI; vault operator init
    uri:
      url: "{{ VAULT_ADDR }}/v1/sys/init"
      method: PUT
      body_format: json
      body:
        secret_shares: 5
        secret_threshold: 3
        stored_shares: 0
        pgp_keys: null
        recovery_shares: 5
        recovery_threshold: 3
        recovery_pgp_keys: null
        root_token_php_key: ""
      validate_certs: false
    register: vault_init

  - name: TEMPLATE; write JSON response to local file
    copy:
      content: "{{ vault_init.json | to_nice_json }}"
      dest: "vault_secrets.json"
    delegate_to: localhost
    connection: local

  - name: VAULT; vault operator unseal (using first 3 keys)
    shell:
      cmd: VAULT_ADDR="{{ VAULT_ADDR }}" vault operator unseal -tls-skip-verify {{ item }}
    loop: "{{ vault_init.json['keys'][0:3] }}"
  when: not vault_status.json.initialized

- name: VAULT; unseal Vault from saved vars if already initialized
  block:
  - name: INCLUDE_VARS; read vars from 'vault_secrets.json' if Vault already initialized
    include_vars:
      file: "vault_secrets.json"
      name: "vault_secrets"

  - name: VAULT; vault operator unseal (using first 3 keys)
    shell:
      cmd: VAULT_ADDR="{{ VAULT_ADDR }}" vault operator unseal -tls-skip-verify {{ item }}
    loop: "{{ vault_secrets['keys'][0:3] }}"
  when: vault_status.json.initialized

- name: GET_URI; check vault status
  uri:
    url: "{{ VAULT_ADDR }}/v1/sys/seal-status"
    method: GET
    validate_certs: false
  register: vault_status

- name: VAULT; vault status
  debug:
    msg:
    - "Vault Initialized: {{ vault_status.json.initialized }}"
    - "Vault Sealed: {{ vault_status.json.sealed }}"
