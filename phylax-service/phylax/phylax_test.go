package phylax

import (
	"testing"
	"time"

	"gitlab.com/cyverse/nafigos/common"
)

var testStartDate = time.Now().UTC().Truncate(time.Millisecond)

var testCluster = common.Cluster{
	ID:               "bqd4sg30ip20n8068ocg",
	Name:             "test_cluster",
	DefaultNamespace: "default",
	Config:           "VGhlIHJlZCBkZWF0aCBoYWQgbG9uZyBkZXZhc3RhdGVkIHRoZSBjb3VudHJ5LiBObyBwZXN0aWxlbmNlIGhhZCBldmVyIGJlZW4gc28gZmF0YWwsIG9yIHNvIGhpZGVvdXMuIEJsb29kIHdhcyBpdHMgQXZhdGFyIGFuZCBpdHMgc2VhbCAtLSB0aGUgbWFkbmVzcyBhbmQgdGhlIGhvcnJvciBvZiBibG9vZC4gVGhlcmUgd2VyZSBzaGFycCBwYWlucywgYW5kIHN1ZGRlbiBkaXp6aW5lc3MsIGFuZCB0aGVuIHByb2Z1c2UgYmxlZWRpbmcgYXQgdGhlIHBvcmVzLCB3aXRoIGRpc3NvbHV0aW9uLiBUaGUgc2NhcmxldCBzdGFpbnMgdXBvbiB0aGUgYm9keSBhbmQgZXNwZWNpYWxseSB1cG9uIHRoZSBmYWNlIG9mIHRoZSB2aWN0aW0sIHdlcmUgdGhlIHBlc3QgYmFuIHdoaWNoIHNodXQgaGltIG91dCBmcm9tIHRoZSBhaWQgYW5kIGZyb20gdGhlIHN5bXBhdGh5IG9mIGhpcyBmZWxsb3ctbWVuLiBBbmQgdGhlIHdob2xlIHNlaXp1cmUsIHByb2dyZXNzLCBhbmQgdGVybWluYXRpb24gb2YgdGhlIGRpc2Vhc2UsIHdlcmUgaW5jaWRlbnRzIG9mIGhhbGYgYW4gaG91ci4KCkJ1dCBQcmluY2UgUHJvc3Blcm8gd2FzIGhhcHB5IGFuZCBkYXVudGxlc3MgYW5kIHNhZ2FjaW91cy4gV2hlbiBoaXMgZG9taW5pb25zIHdlcmUgaGFsZiBkZXBvcHVsYXRlZCwgaGUgc3VtbW9uZWQgdG8gaGlzIHByZXNlbmNlIGEgdGhvdXNhbmQgaGFsZSBhbmQgbGlnaHQtaGVhcnRlZCBmcmllbmRzIGZyb20gYW1vbmcgdGhlIGtuaWdodHMgYW5kIGRhbWVzIG9mIGhpcyBjb3VydCwgYW5kIHdpdGggdGhlc2UgcmV0aXJlZCB0byB0aGUgZGVlcCBzZWNsdXNpb24gb2Ygb25lIG9mIGhpcyBjcmVuZWxsYXRlZCBhYmJleXMuIFRoaXMgd2FzIGFuIGV4dGVuc2l2ZSBhbmQgbWFnbmlmaWNlbnQgc3RydWN0dXJlLCB0aGUgY3JlYXRpb24gb2YgdGhlIHByaW5jZSdzIG93biBlY2NlbnRyaWMgeWV0IGF1Z3VzdCB0YXN0ZS4gQSBzdHJvbmcgYW5kIGxvZnR5IHdhbGwgZ2lyZGxlZCBpdCBpbi4gVGhpcyB3YWxsIGhhZCBnYXRlcyBvZiBpcm9uLiBUaGUgY291cnRpZXJzLCBoYXZpbmcgZW50ZXJlZCwgYnJvdWdodCBmdXJuYWNlcyBhbmQgbWFzc3kgaGFtbWVycyBhbmQgd2VsZGVkIHRoZSBib2x0cy4=",
}

var testUser = common.User{
	Username: "test_user",
	Clusters: map[string]common.Cluster{
		"bqd4sg30ip20n8068ocg": testCluster,
	},
}

type testMap map[string]func(t *testing.T)

func TestRunner(t *testing.T) {
	tests := testMap{
		"CreateRunner":                testCreateRunner,
		"GetRunnersForUser":           testGetRunnersForUser,
		"TestGetRunner":               testGetRunner,
		"TestGetRunnersNotAuthorized": testGetRunnerNotAuthorized,
	}

	for name, test := range tests {
		t.Run(name, test)
	}
}

func TestBuild(t *testing.T) {
	tests := testMap{
		"CreateBuild":                testCreateBuild,
		"GetBuildsForUser":           testGetBuildsForUser,
		"TestGetBuild":               testGetBuild,
		"TestGetBuildsNotAuthorized": testGetBuildNotAuthorized,
	}

	for name, test := range tests {
		t.Run(name, test)
	}
}
