package phylax

import (
	"encoding/json"
	"fmt"

	"gitlab.com/cyverse/nafigos/common"

	"github.com/nats-io/nats.go"
	log "github.com/sirupsen/logrus"
	core "k8s.io/api/core/v1"
	meta "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// Request is a struct containing the necessary fields for all Phylax
// operations coming from the API
type Request struct {
	common.BasicRequest
	RunAfterBuild      bool               `json:"run"`
	TCPPorts           []core.ServicePort `json:"tcp_ports,omitempty"`
	HTTPPorts          []core.ServicePort `json:"http_ports,omitempty"`
	Namespace          string             `json:"namespace,omitempty"`
	AuthDisabled       bool               `json:"auth_disabled,omitempty"`
	WorkflowDefinition struct {
		Name       string             `json:"name,omitempty"`
		Type       string             `bson:"type" json:"type,omitempty" yaml:"type"`
		TCPPorts   []core.ServicePort `json:"tcp_ports,omitempty"`
		Repository common.Repository  `json:"repository"`
	} `json:"data,omitempty"`
}

func errorResponse(errMsg string, code int) ([]byte, error) {
	return json.Marshal(struct {
		Error  common.Error `json:"error,omitempty"`
		Runner *Runner      `json:"runner,omitempty"`
	}{
		Error:  common.Error{Message: errMsg, Code: code},
		Runner: nil,
	})
}

// GetRunner will return the Runner from MongoDB with the information tying it to a WorkflowDefinition
func GetRunner(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
	logger := log.WithFields(log.Fields{
		"package":  "phylax",
		"function": "GetRunner",
	})

	var request Request
	err := json.Unmarshal(reqBytes, &request)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to read request to get Runner")
		response, err := errorResponse(fmt.Sprintf("unable to read request to get Runner: %v", err), 400)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to create response message")
		}
		msg.Respond(response)
		return
	}
	runner, err := db.GetRunner(request.GetRunID(), request.GetUser().Username)

	logRequest := request
	logRequest.BasicRequest = logRequest.GetRedacted()
	logger = logger.WithFields(log.Fields{"request": logRequest})

	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to get Runner")
		response, err := errorResponse(fmt.Sprintf("unable to get Runner '%s': %v", request.GetRunID(), err), 500)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to create response message")
		}
		msg.Respond(response)
		return
	}

	logRunner := runner.GetRedacted()
	logger = logger.WithFields(log.Fields{"runner": logRunner})
	logger.Info("successfully got Runner")

	data, err := json.Marshal(struct {
		Runner *Runner `json:"runner"`
	}{
		Runner: runner,
	})
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to marshal Runner response")
	}
	msg.Respond(data)
}

// GetRunnersForUser will return all Runners from MongoDB with information tying them to WorkflowDefinitions
func GetRunnersForUser(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
	logger := log.WithFields(log.Fields{
		"package":  "phylax",
		"function": "GetRunnersForUser",
	})

	var request Request
	err := json.Unmarshal(reqBytes, &request)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to read request to get Runners")
		response, err := errorResponse(fmt.Sprintf("unable to read request to get Runners: %v", err), 400)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to create response message")
		}
		msg.Respond(response)
		return
	}

	logRequest := request
	logRequest.BasicRequest = logRequest.GetRedacted()
	logger = logger.WithFields(log.Fields{"request": logRequest})

	runners, err := db.GetRunnersForUser(request.GetUser().Username)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to get Runners")
		response, err := errorResponse(fmt.Sprintf("unable to get Runners for '%s': %v", request.GetUser().Username, err), 500)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to create response message")
		}
		msg.Respond(response)
		return
	}

	var logRunners []Runner
	for _, runner := range runners {
		logRunners = append(logRunners, runner.GetRedacted())
	}
	logger = logger.WithFields(log.Fields{"runners": logRunners})
	logger.Info("successfully got Runners")

	data, jsonErr := json.Marshal(struct {
		Runners []Runner `json:"runners"`
	}{
		Runners: runners,
	})
	if jsonErr != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to marshal Runners response")
	}
	msg.Respond(data)
}

// UpdateRunner is used to add the TCPPort NodePort to the Runner after it is created
func UpdateRunner(reqBytes []byte, natsInfo map[string]string) {
	logger := log.WithFields(log.Fields{
		"package":  "phylax",
		"function": "UpdateRunner",
	})

	var request Request
	err := json.Unmarshal(reqBytes, &request)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to read request to update runner")
		return
	}

	logRequest := request
	logRequest.BasicRequest = logRequest.GetRedacted()
	logger = logger.WithFields(log.Fields{"request": logRequest})

	runner, err := db.GetRunner(request.GetRunID(), request.GetUser().Username)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to get Runner before updating")
		return
	}

	logRunner := runner.GetRedacted()
	logger = logger.WithFields(log.Fields{"runner": logRunner})
	logger.Info("successfully got Runner")

	runner.TCPPorts = request.TCPPorts
	runner.HTTPPorts = request.HTTPPorts
	err = db.Save(runner)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to save Runner after updating")
		return
	}
}

// GetResources ...
func GetResources(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
	logger := log.WithFields(log.Fields{
		"package":  "phylax",
		"function": "GetResources",
	})

	var request Request
	err := json.Unmarshal(reqBytes, &request)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to read request to get resources")
		response, err := errorResponse(fmt.Sprintf("unable to read request to get resources: %v", err), 400)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to create response message")
		}
		msg.Respond(response)
		return
	}

	logRequest := request
	logRequest.BasicRequest = logRequest.GetRedacted()
	logger = logger.WithFields(log.Fields{"request": logRequest})

	cluster, ok := request.User.Clusters[request.GetClusterID()]
	if !ok {
		err = fmt.Errorf("error getting cluster config")
		logger.WithFields(log.Fields{"error": err}).Error("unable to get cluster config required to get resources")
		return
	}

	if len(request.Namespace) == 0 {
		request.Namespace = cluster.DefaultNamespace
	}

	clientsets, err := common.NewUserClusterK8sClientsets(cluster.Config)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to get dynamic clientset to access cluster")
		response, err := errorResponse(fmt.Sprintf("unable to get dynamic clientset to access cluster: %v", err), 500)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to create response message")
		}
		msg.Respond(response)
		return
	}

	pods, err := clientsets.ListPods(meta.ListOptions{}, request.Namespace)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to get pod list")
		response, err := errorResponse(fmt.Sprintf("unable to get pod list: %v", err), 500)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to create response message")
		}
		msg.Respond(response)
		return
	}
	type podInfo struct {
		PodName   string        `json:"pod_name"`
		NafigosID string        `json:"nafigos_id"`
		Namespace string        `json:"namespace"`  // (meta.Namespace)
		Phase     core.PodPhase `json:"phase"`      // (Status.Phase)
		StartTime *meta.Time    `json:"start_time"` // (Status.StartTime)
	}
	podResult := []podInfo{}
	for _, pod := range pods.Items {
		podResult = append(podResult, podInfo{
			PodName:   pod.ObjectMeta.Name,
			NafigosID: pod.ObjectMeta.Labels["nafigos_id"],
			Namespace: pod.ObjectMeta.Namespace,
			Phase:     pod.Status.Phase,
			StartTime: pod.Status.StartTime,
		})
	}
	logger = logger.WithFields(log.Fields{"pods": podResult})
	logger.Info("successfully got pods")

	deployments, err := clientsets.ListDeployments(meta.ListOptions{}, request.Namespace)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to get deployment list")
		response, err := errorResponse(fmt.Sprintf("unable to get deployment list: %v", err), 500)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to create response message")
		}
		msg.Respond(response)
		return
	}
	type deploymentInfo struct {
		DeploymentName    string     `json:"deployment_name"`
		NafigosID         string     `json:"nafigos_id"`
		Namespace         string     `json:"namespace"`
		Replicas          int32      `json:"replicas"`
		ReadyReplicas     int32      `json:"ready_replicas"`
		AvailableReplicas int32      `json:"available_replicas"`
		CreationTime      meta.Time  `json:"creation_time"`
		DeletionTime      *meta.Time `json:"deletion_time"`
	}
	deploymentResult := []deploymentInfo{}
	for _, deployment := range deployments.Items {
		deploymentResult = append(deploymentResult, deploymentInfo{
			DeploymentName:    deployment.ObjectMeta.Name,
			NafigosID:         deployment.ObjectMeta.Labels["nafigos_id"],
			Namespace:         deployment.ObjectMeta.Namespace,
			Replicas:          deployment.Status.Replicas,
			ReadyReplicas:     deployment.Status.ReadyReplicas,
			AvailableReplicas: deployment.Status.AvailableReplicas,
			CreationTime:      deployment.ObjectMeta.CreationTimestamp,
			DeletionTime:      deployment.ObjectMeta.DeletionTimestamp,
		})
	}
	logger = logger.WithFields(log.Fields{"deployments": deploymentResult})
	logger.Info("successfully got deployments")

	jobs, err := clientsets.ListJobs(meta.ListOptions{}, request.Namespace)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to get jobs list")
		response, err := errorResponse(fmt.Sprintf("unable to get jobs list: %v", err), 500)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to create response message")
		}
		msg.Respond(response)
		return
	}
	type jobInfo struct {
		JobName        string     `json:"job_name"`
		NafigosID      string     `json:"nafigos_id"`
		Namespace      string     `json:"namespace"`
		Active         int32      `json:"active"`
		Succeeded      int32      `json:"succeeded"`
		Failed         int32      `json:"failed"`
		Completions    *int32     `json:"completions"`
		StartTime      *meta.Time `json:"start_time"`
		CompletionTime *meta.Time `json:"completion_time"`
	}
	jobResult := []jobInfo{}
	for _, job := range jobs.Items {
		jobResult = append(jobResult, jobInfo{
			JobName:        job.ObjectMeta.Name,
			NafigosID:      job.ObjectMeta.Labels["nafigos_id"],
			Namespace:      job.ObjectMeta.Namespace,
			Active:         job.Status.Active,
			Succeeded:      job.Status.Succeeded,
			Failed:         job.Status.Failed,
			Completions:    job.Spec.Completions,
			StartTime:      job.Status.StartTime,
			CompletionTime: job.Status.CompletionTime,
		})
	}
	logger = logger.WithFields(log.Fields{"jobs": jobResult})
	logger.Info("successfully got jobs")

	argoWorkflows, err := clientsets.ListArgoWorkflows(meta.ListOptions{}, request.Namespace)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to get argo workflows list")
		response, err := errorResponse(fmt.Sprintf("unable to get argo workflows list: %v", err), 500)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to create response message")
		}
		msg.Respond(response)
		return
	}
	type workflowsInfo struct {
		WorkflowName string    `json:"workflow_name"`
		NafigosID    string    `json:"nafigos_id"`
		Namespace    string    `json:"namespace"`
		Phase        string    `json:"phase"`
		StartedAt    meta.Time `json:"started_at"`
		FinishedAt   meta.Time `json:"finished_at"`
	}
	workflowResult := []workflowsInfo{}
	for _, workflow := range argoWorkflows.Items {
		workflowResult = append(workflowResult, workflowsInfo{
			WorkflowName: workflow.ObjectMeta.Name,
			NafigosID:    workflow.ObjectMeta.Labels["nafigos_id"],
			Namespace:    workflow.ObjectMeta.Namespace,
			Phase:        string(workflow.Status.Phase),
			StartedAt:    workflow.Status.StartedAt,
			FinishedAt:   workflow.Status.FinishedAt,
		})
	}
	logger = logger.WithFields(log.Fields{"argo_workflows": workflowResult})
	logger.Info("successfully got argo workflows")

	data, err := json.Marshal(struct {
		Pods          []podInfo        `json:"pods"`
		Deployments   []deploymentInfo `json:"deployments"`
		Jobs          []jobInfo        `json:"jobs"`
		ArgoWorkflows []workflowsInfo  `json:"argo_workflows"`
	}{
		Pods:          podResult,
		Deployments:   deploymentResult,
		Jobs:          jobResult,
		ArgoWorkflows: workflowResult,
	})
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to marshal response")
	}
	msg.Respond(data)
}

// GetBuild will return the Build from MongoDB with the information tying it to a WorkflowDefinition
func GetBuild(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
	logger := log.WithFields(log.Fields{
		"package":  "phylax",
		"function": "GetBuild",
	})
	var request Request
	err := json.Unmarshal(reqBytes, &request)
	if err != nil {
		response, err := errorResponse(fmt.Sprintf("Error reading request to get Build: %v\n", err), 400)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to create response message")
		}
		msg.Respond(response)
		return
	}
	build, err := db.GetBuild(request.GetBuildID(), request.GetUser().Username)

	if err != nil {
		response, err := errorResponse(fmt.Sprintf("Error getting Build '%s': %v", request.GetRunID(), err), 500)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to create response message")
		}
		msg.Respond(response)
		return
	}

	log.Printf("Responding with Build '%s'\n", request.GetRunID())
	data, err := json.Marshal(struct {
		Build *Build `json:"build"`
	}{
		Build: build,
	})
	if err != nil {
		log.Println("Error encoding JSON:", err)
	}
	msg.Respond(data)
}

// GetBuildsForUser will return all Builds from MongoDB with information tying them to WorkflowDefinitions
func GetBuildsForUser(reqBytes []byte, msg *nats.Msg, natsInfo map[string]string) {
	logger := log.WithFields(log.Fields{
		"package":  "phylax",
		"function": "GetBuildsForUser",
	})
	var request Request
	err := json.Unmarshal(reqBytes, &request)
	if err != nil {
		response, err := errorResponse(fmt.Sprintf("Error reading request to get Build for user: %v\n", err), 400)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to create response message")
		}
		msg.Respond(response)
		return
	}

	builds, err := db.GetBuildsForUser(request.GetUser().Username)
	if err != nil {
		response, err := errorResponse(fmt.Sprintf("Error getting Builds for '%s': %v", request.GetUser().Username, err), 500)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to create response message")
		}
		msg.Respond(response)
		return
	}

	log.Printf("Responding with %d Builds\n", len(builds))
	data, jsonErr := json.Marshal(struct {
		Builds []Build `json:"builds"`
	}{
		Builds: builds,
	})
	if jsonErr != nil {
		log.Println("Error encoding JSON:", err)
	}
	msg.Respond(data)
}

// // Get ...
// // This is dead code at the moment but it may be usefule in the future for retrieving
// // the status of Kubernetes resources on the user cluster
// func Get(request *common.Request, msg *nats.Msg, natsInfo map[string]string) {
// 	cluster, ok := request.GetUser().Clusters[request.ClusterID]
// 	if !ok {
// 		log.Printf("Error getting cluster config '%s'", request.ClusterID)
// 		return
// 	}
// 	// Set namespaceName if specified, otherwise use default for the Cluster
// 	namespaceName := request.Namespace
// 	if len(namespaceName) == 0 {
// 		namespaceName = cluster.DefaultNamespace
// 	}
// 	clientsets, err := common.NewUserClusterK8sClientsets(cluster.Config)
// 	if err != nil {
// 		log.Println("Error getting clientsets:", err)
// 		return
// 	}

// 	status := struct {
// 		WorkflowType string `json:"workflowtype"`
// 		Status       string `json:"status"`
// 		Message      string `json:"message"`
// 	}{}

// 	name := common.CreateResourceName(request.GetUser().Username, request.Name, request.ID)
// 	if request.Type == "argo" {
// 		workflow, err := clientsets.GetArgoWorkflow(name, namespaceName)
// 		if err != nil {
// 			log.Printf("Error getting Workflow %s\n", name)
// 			return
// 		}

// 		status.Status = string(workflow.Status.Phase)
// 		status.WorkflowType = "argo"
// 		status.Message = workflow.Status.Message
// 	} else {
// 		deployment, err := clientsets.GetDeployment(name, namespaceName)
// 		if err != nil {
// 			log.Printf("Error getting deployment %s: %v\n", name, err)
// 			return
// 		}

// 		conditions := deployment.Status.Conditions
// 		latestCondition := conditions[len(conditions)-1]

// 		status.Status = string(latestCondition.Type)
// 		status.Message = latestCondition.Message
// 		status.WorkflowType = "default"
// 	}

// 	statusBytes, err := json.Marshal(status)
// 	if err != nil {
// 		log.Printf("Error marshalling JSON response %s\n", err)
// 		return
// 	}
// 	msg.Respond(statusBytes)
// }
