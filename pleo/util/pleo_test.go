package util

import (
	"testing"

	"gitlab.com/cyverse/nafigos/common"

	"github.com/stretchr/testify/assert"
	core "k8s.io/api/core/v1"
)

func TestCreateDestinationRule(t *testing.T) {
	clientsets, err := common.NewTestK8sClientsets()
	assert.NoError(t, err)
	destinationRule := createDestinationRule("foo", "ID", "master")
	_, err = clientsets.CreateDestinationRule(destinationRule, "default")
	assert.NoError(t, err)
	assert.Equal(t, "foo", destinationRule.ObjectMeta.Labels["app"])
	assert.Equal(t, "master", destinationRule.ObjectMeta.Labels["version"])
	assert.Equal(t, "ID", destinationRule.ObjectMeta.Labels["nafigos_id"])
}

func TestCreateDeployment(t *testing.T) {
	clientsets, err := common.NewTestK8sClientsets()
	assert.NoError(t, err)

	deployment := createDeployment([]core.Container{{
		Name:  "test",
		Image: "ubuntu",
		Env: []core.EnvVar{
			{
				Name:  "FOO",
				Value: "BAR",
			},
		},
		Ports: []core.ContainerPort{
			{ContainerPort: int32(80)},
		},
	}}, "ID", "foo", "master")
	_, err = clientsets.CreateDeployment(deployment, "default")
	assert.NoError(t, err)
	assert.Equal(t, "foo", deployment.ObjectMeta.Labels["app"])
	assert.Equal(t, "master", deployment.ObjectMeta.Labels["version"])
	assert.Equal(t, "ID", deployment.ObjectMeta.Labels["nafigos_id"])
}

func TestCreateService(t *testing.T) {
	clientsets, err := common.NewTestK8sClientsets()
	assert.NoError(t, err)

	ports := []core.ServicePort{{Port: int32(80), Name: "http"}}
	service := createService("test", "ID", "1.0", ports)
	_, err = clientsets.CreateService(service, "default")
	assert.NoError(t, err)
	assert.Equal(t, "test", service.ObjectMeta.Labels["app"])
	assert.Equal(t, "1.0", service.ObjectMeta.Labels["version"])
	assert.Equal(t, "ID", service.ObjectMeta.Labels["nafigos_id"])
}

func TestCreateVirtualService(t *testing.T) {
	clientsets, err := common.NewTestK8sClientsets()
	assert.NoError(t, err)

	ports := []core.ServicePort{{Port: int32(80), Name: "http"}}
	virtualService := createVirtualService("test", "*", "1.0", "ID", ports, ports)
	_, err = clientsets.CreateVirtualService(virtualService, "default")
	assert.NoError(t, err)
	assert.Equal(t, "test", virtualService.ObjectMeta.Labels["app"])
	assert.Equal(t, "1.0", virtualService.ObjectMeta.Labels["version"])
	assert.Equal(t, "ID", virtualService.ObjectMeta.Labels["nafigos_id"])
}

func TestCreateScaledObject(t *testing.T) {
	clientsets, err := common.NewTestK8sClientsets()
	assert.NoError(t, err)

	deployment := createDeployment([]core.Container{{
		Name:  "test",
		Image: "ubuntu",
		Env: []core.EnvVar{
			{
				Name:  "FOO",
				Value: "BAR",
			},
		},
		Ports: []core.ContainerPort{
			{ContainerPort: int32(80)},
		},
	}}, "foo", "ID", "master")
	_, err = clientsets.CreateDeployment(deployment, "default")
	assert.NoError(t, err)
	scaledObject := GetUnstructuredScaledObject("test", "default")

	_, err = clientsets.CreateScaledObject(scaledObject, "default")
	assert.NoError(t, err)
}
