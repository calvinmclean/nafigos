package util

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/base64"
	"encoding/pem"
	"time"

	"gitlab.com/cyverse/nafigos/common"

	certificates "k8s.io/api/certificates/v1beta1"
	rbac "k8s.io/api/rbac/v1"
	meta "k8s.io/apimachinery/pkg/apis/meta/v1"
	api "k8s.io/client-go/tools/clientcmd/api/v1"
	"sigs.k8s.io/yaml"
)

func createCertificateRequest(slug string, key *rsa.PrivateKey) ([]byte, error) {
	certRequest := x509.CertificateRequest{
		Subject:            pkix.Name{CommonName: slug},
		SignatureAlgorithm: x509.SHA256WithRSA,
	}
	csrBytes, err := x509.CreateCertificateRequest(rand.Reader, &certRequest, key)
	if err != nil {
		return nil, err
	}
	return pem.EncodeToMemory(&pem.Block{
		Type:  "CERTIFICATE REQUEST",
		Bytes: csrBytes,
	}), nil
}

func createK8sCSR(slug string, certificateRequest []byte) certificates.CertificateSigningRequest {
	return certificates.CertificateSigningRequest{
		ObjectMeta: meta.ObjectMeta{Name: slug},
		Spec: certificates.CertificateSigningRequestSpec{
			Request: certificateRequest,
			Groups:  []string{"system:authenticated"},
			Usages:  []certificates.KeyUsage{certificates.UsageClientAuth},
		},
	}
}

func createK8sCSRApproval(slug string) certificates.CertificateSigningRequest {
	return certificates.CertificateSigningRequest{
		ObjectMeta: meta.ObjectMeta{Name: slug},
		Status: certificates.CertificateSigningRequestStatus{
			Conditions: []certificates.CertificateSigningRequestCondition{
				{
					Type:   certificates.CertificateApproved,
					Reason: "ApprovedByMyPolicy",
				},
			},
		},
	}
}

func createUserClusterRoleBinding(slug, roleRefName string) rbac.RoleBinding {
	return rbac.RoleBinding{
		ObjectMeta: meta.ObjectMeta{
			Name:      slug + "-" + roleRefName,
			Namespace: slug,
		},
		RoleRef: rbac.RoleRef{
			APIGroup: "rbac.authorization.k8s.io",
			Kind:     "ClusterRole",
			Name:     roleRefName,
		},
		Subjects: []rbac.Subject{{
			APIGroup: "rbac.authorization.k8s.io",
			Kind:     "User",
			Name:     slug,
		}},
	}
}

func createNafigosClusterRole() rbac.ClusterRole {
	return rbac.ClusterRole{
		ObjectMeta: meta.ObjectMeta{Name: "nafigos"},
		Rules: []rbac.PolicyRule{
			{
				APIGroups: []string{""},
				Resources: []string{"configmaps", "endpoints", "namespaces", "nodes", "pods", "pods/log", "replicationcontrollers", "services"},
				Verbs:     []string{"get", "list", "watch"},
			},
			{
				APIGroups: []string{"extensions", "apps"},
				Resources: []string{"deployments", "replicasets", "statefulsets"},
				Verbs:     []string{"get", "list", "watch"},
			},
			{
				APIGroups: []string{"autoscaling"},
				Resources: []string{"horizontalpodautoscalers"},
				Verbs:     []string{"get", "list", "watch"},
			},
			{
				APIGroups: []string{"batch"},
				Resources: []string{"cronjobs", "jobs"},
				Verbs:     []string{"get", "list", "watch"},
			},
			{
				APIGroups: []string{"config.istio.io", "networking.istio.io", "authentication.istio.io", "rbac.istio.io", "security.istio.io"},
				Resources: []string{"*"},
				Verbs:     []string{"create", "delete", "get", "list", "patch", "watch"},
			},
			{
				APIGroups: []string{"monitoring.kiali.io"},
				Resources: []string{"monitoringdashboards"},
				Verbs:     []string{"get", "list"},
			},
			{
				APIGroups: []string{""},
				Resources: []string{"pods", "services", "services/finalizers", "endpoints", "events", "configmaps", "secrets", "namespaces", "external"},
				Verbs:     []string{"*"},
			},
			{
				APIGroups: []string{"apps"},
				Resources: []string{"deployments", "deployments/finalizers", "deployments/finalizers"},
				Verbs:     []string{"*"},
			},
			{
				APIGroups: []string{"batch"},
				Resources: []string{"jobs"},
				Verbs:     []string{"*"},
			},
			{
				APIGroups: []string{"monitoring.coreos.com"},
				Resources: []string{"servicemonitors"},
				Verbs:     []string{"get", "create"},
			},
			{
				APIGroups: []string{"keda.k8s.io"},
				Resources: []string{"*"},
				Verbs:     []string{"*"},
			},
			{
				APIGroups: []string{"autoscaling"},
				Resources: []string{"horizontalpodautoscalers"},
				Verbs:     []string{"*"},
			},
			{
				APIGroups: []string{"openfaas.com"},
				Resources: []string{"functions"},
				Verbs:     []string{"get", "list", "watch", "create", "update", "patch", "delete"},
			},
		},
	}
}

func createUserConfig(adminB64Kubeconfig string, certData []byte, key *rsa.PrivateKey, slug string) (config api.Config, err error) {
	// Decode Base64 admin kubeconfig and unmarshal into struct
	decodedKubeconfig, err := base64.StdEncoding.DecodeString(adminB64Kubeconfig)
	if err != nil {
		return
	}
	err = yaml.Unmarshal([]byte(decodedKubeconfig), &config)
	if err != nil {
		return
	}

	// Encode the key
	keyData := pem.EncodeToMemory(&pem.Block{
		Type:  "PRIVATE KEY",
		Bytes: x509.MarshalPKCS1PrivateKey(key),
	})

	// Overwrite some fields of the admin config with the new cert and key
	config.Clusters[0].Name = "cyverse-user-cluster"
	config.AuthInfos = []api.NamedAuthInfo{
		{
			Name: slug,
			AuthInfo: api.AuthInfo{
				ClientCertificateData: certData,
				ClientKeyData:         keyData,
			},
		},
	}
	config.Contexts = []api.NamedContext{
		{
			Name: slug,
			Context: api.Context{
				Cluster:  "cyverse-user-cluster",
				AuthInfo: slug,
			},
		},
	}
	config.CurrentContext = slug
	return
}

func createNewUserClusterConfig(slug string, clientset *common.K8sClientsets) (string, error) {
	// Create a new private key
	key, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil {
		return "", err
	}
	// Create new CertificateSigningRequest
	certificateRequest, err := createCertificateRequest(slug, key)
	if err != nil {
		return "", err
	}
	k8sCSR := createK8sCSR(slug, certificateRequest)
	_, err = clientset.CreateCertificateSigningRequest(&k8sCSR)
	if err != nil {
		return "", err
	}
	// Approve the request
	approval := createK8sCSRApproval(slug)
	_, err = clientset.ApproveCertificateSigningRequest(&approval)
	if err != nil {
		return "", err
	}
	// Now get the approved request to read Cert
	time.Sleep(500 * time.Millisecond)
	certificate, err := clientset.GetCertificateSigningRequest(slug)
	if err != nil {
		return "", err
	}
	// Create RoleBindings for regular resources, KEDA, and Istio
	adminRoleBinding := createUserClusterRoleBinding(slug, "admin")
	_, err = clientset.CreateRoleBinding(&adminRoleBinding, slug)
	if err != nil {
		return "", err
	}
	nafigosClusterRole := createNafigosClusterRole()
	_, err = clientset.CreateClusterRole(&nafigosClusterRole)
	if err != nil && err.Error() != `clusterroles.rbac.authorization.k8s.io "nafigos" already exists` {
		return "", err
	}
	nafigosRoleBinding := createUserClusterRoleBinding(slug, "nafigos")
	_, err = clientset.CreateRoleBinding(&nafigosRoleBinding, slug)
	if err != nil {
		return "", err
	}
	// Now create the user config struct using some info from admin config
	// and convert to base64 encoded string
	userConfig, err := createUserConfig(adminKubeconfig, certificate.Status.Certificate, key, slug)
	if err != nil {
		return "", err
	}
	configBytes, err := yaml.Marshal(userConfig)
	if err != nil {
		return "", err
	}
	return base64.StdEncoding.EncodeToString(configBytes), nil
}
